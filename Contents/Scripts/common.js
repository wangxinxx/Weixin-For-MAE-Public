﻿//
function ShowTips(t,html,timeout,fn){
	if(!document.getElementById("TipsDialog")){
		var TipsDialog = document.createElement("div");
		TipsDialog.id="TipsDialog";
		document.body.appendChild(TipsDialog);
	}
	if(typeof timeout=="function"){
		fn = timeout;
		timeout=0;
	}else{
		timeout = timeout ||0;
	}
	if(typeof fn!="function")fn=function(){};
	F("#TipsDialog").attr("title",t).html("<div style=\"text-align:center;padding:20px 0;\">"+html+"</div>").dialog({
		"width":250,"height":200,"mask":false,buttons:{
			"确定":fn
		}
	});	
	if(timeout>0)window.setTimeout(function(){F("#TipsDialog").dialog("close");}, timeout);
}

function ShowTipsTop(html,timeout,fn){
	if(!document.getElementById("TipsDialogTop")){
		var TipsDialog = document.createElement("div");
		TipsDialog.id="TipsDialogTop";
		TipsDialog.className="TipsDialogTop TipsDialogTop-" + this;
		document.body.appendChild(TipsDialog);
	}
	if(typeof timeout=="function"){
		fn = timeout;
		timeout=2000;
	}else{
		timeout = timeout ||2000;
	}
	if(typeof fn!="function")fn=function(){};
	F("#TipsDialogTop").html(html.replace(/</igm,"&lt;").replace(/>/igm,"&gt;")).css({"display":"block"});
	if(timeout>0)window.setTimeout((function(fn){return function(){F("#TipsDialogTop").css({"display":"none"});fn();}})(fn), timeout);
}
function CloseTips(timeout){
	timeout = timeout ||0;
	if(timeout<=0)F("#TipsDialog").dialog("close");
	else window.setTimeout(function(){F("#TipsDialog").dialog("close");}, timeout);
}
function SendMessage(frm){
	Ajax({
		form:frm,
		succeed:function(msg){
			ShowTips("提示",msg,1000);
			window.location.reload();
		}
	});
}
function ShowMediaMessage(Id,t){
	Ajax({
		url:"?m=MediaMessages&a=edit&Id=" + Id +"&type=" + (t||"text"),
		succeed:function(msg){
			F("#DialogAlert").attr("title","常用信息").html(msg).dialog({
				"width":575,"height":500,afterOpen:function(){
						new wechatEditor('message_intro', {emotionPath : '/Contents/Emotions/'});
					},buttons:{
					"保存消息":function(){
						SendMessage(document.forms['frm_sendmessage']);
						return false;
					}
				}
			});
		}
	});
}
function ShowReply(OpenId,t){
	Ajax({
		url:"?m=MessagesReply&OpenId=" + OpenId +"&type=" + (t||"text"),
		succeed:function(msg){
			F("#DialogAlert").attr("title","回复信息").html(msg).dialog({
				"width":575,"height":500,afterOpen:function(){
						new wechatEditor('message_intro', {emotionPath : '/Contents/Emotions/'});
					},buttons:{
					"立即回复消息":function(){
						ShowTips("提示","正在发送消息...");
						Ajax({
							form:document.forms['frm_sendmessage'],
							succeed:function(msg){
								ShowTips("提示",msg,1000);
							}
						});
					}
				}
			});
		}
	});
}
function FetchImageFromUrl(url,src){
	F("#img_list_from_news").html("正在抓取。。。");
	Ajax({
		url:"?m=MediaMessages&a=getimagesfromurl&url=" + F.encode(url),
		succeed:function(msg){
			if(msg==""){
				F("#img_list_from_news").html("没有找到任何图片。");
			}else{
				var all="";
				var list = msg.split("|");
				for(var i =0;i<list.length;i++){
					if(list[i]=="")continue;
					all += "<img src=\"" + list[i] + "\" width=\"24\" height=\"24\" onclick=\"document.forms['frm_sendmessage']['PicUrl'].value=this.src;\" /> ";
				}
				F("#img_list_from_news").html(all);
			}
		}
	});	
}
function ParseUrl(url,src){
	F("#img_list_from_news").html("正在抓取。。。");
	Ajax({
		url:"?m=MediaMessages&a=parseurl&url=" + F.encode(url),
		dataType:"json",
		succeed:function(msg){
			if(!msg)F("#img_list_from_news").html("获取失败");
			else{
				document.forms['frm_sendmessage']['Title'].value=msg["Title"];
				document.forms['frm_sendmessage']['Content'].value=msg["Description"];
				var all="";
				var list = msg["All"].split("|");
				for(var i =0;i<list.length;i++){
					if(list[i]=="")continue;
					all += "<img src=\"" + list[i] + "\" width=\"24\" height=\"24\" onclick=\"document.forms['frm_sendmessage']['PicUrl'].value=this.src;\" /> ";
				}
				F("#img_list_from_news").html(all);
			}
		}
	});	
}
function MoveUp(sel){
	for(var i=0;i<sel.length;i++){
		if(sel.options[i].selected){
			if(i==0)continue;
			sel.insertBefore(sel.removeChild(sel.options[i]),sel.options[i-1]);
		}
	}
}
function MoveDown(sel){
	for(var i=sel.length-1;i>=0;i--){
		if(sel.options[i].selected){
			if(i==sel.length-1)continue;
			sel.insertBefore(sel.removeChild(sel.options[i+1]),sel.options[i]);
		}
	}
}