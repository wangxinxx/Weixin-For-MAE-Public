/**
 * 微信表情
 * file		图片文件名
 * name		表情名称
 * code		表情值
 * codeCn	表情值（中文形式）
 * position	表情图 sprite 位置
 * reg		正则模式
 */
(function(){
	var emotions = [
		{file : '0.gif', name : '微笑', code : '/::)', position : '0 0'},
		{file : '1.gif', name : '撇嘴', code : '/::~', position : '-24px 0'},
		{file : '2.gif', name : '色', code : '/::B', position : '-48px 0'},
		{file : '3.gif', name : '发呆', code : '/::|', position : '-72px 0'},
		{file : '4.gif', name : '得意', code : '/:8-)', position : '-96px 0'},
		{file : '5.gif', name : '流泪', code : '/::<', position : '-120px 0'},
		{file : '6.gif', name : '害羞', code : '/::$', position : '-144px 0'},
		{file : '7.gif', name : '闭嘴', code : '/::X', position : '-168px 0'},
		{file : '8.gif', name : '睡', code : '/::Z', position : '-192px 0'},
		{file : '9.gif', name : '大哭', code : '/::\'(', position : '-216px 0'},
		{file : '10.gif', name : '尴尬', code : '/::-|', position : '-240px 0'},
		{file : '11.gif', name : '发怒', code : '/::@', position : '-264px 0'},
		{file : '12.gif', name : '调皮', code : '/::P', position : '-288px 0'},
		{file : '13.gif', name : '呲牙', code : '/::D', position : '-312px 0'},
		{file : '14.gif', name : '惊讶', code : '/::O', position : '-336px 0'},
		{file : '15.gif', name : '难过', code : '/::(', position : '-360px 0'},
		{file : '16.gif', name : '酷', code : '/::+', position : '-384px 0'},
		{file : '17.gif', name : '冷汗', code : '/:--b', position : '-408px 0'},
		{file : '18.gif', name : '抓狂', code : '/::Q', position : '-432px 0'},
		{file : '19.gif', name : '吐', code : '/::T', position : '-456px 0'},
		{file : '20.gif', name : '偷笑', code : '/:,@P', position : '-480px 0'},
		{file : '21.gif', name : '可爱', code : '/:,@-D', position : '-504px 0'},
		{file : '22.gif', name : '白眼', code : '/::d', position : '-528px 0'},
		{file : '23.gif', name : '傲慢', code : '/:,@o', position : '-552px 0'},
		{file : '24.gif', name : '饥饿', code : '/::g', position : '-576px 0'},
		{file : '25.gif', name : '困', code : '/:|-)', position : '-600px 0'},
		{file : '26.gif', name : '惊恐', code : '/::!', position : '-624px 0'},
		{file : '27.gif', name : '流汗', code : '/::L', position : '-648px 0'},
		{file : '28.gif', name : '憨笑', code : '/::>', position : '-672px 0'},
		{file : '29.gif', name : '大兵', code : '/::,@', position : '-696px 0'},
		{file : '30.gif', name : '奋斗', code : '/:,@f', position : '-720px 0'},
		{file : '31.gif', name : '咒骂', code : '/::-S', position : '-744px 0'},
		{file : '32.gif', name : '疑问', code : '/:?', position : '-768px 0'},
		{file : '33.gif', name : '嘘', code : '/:,@x', position : '-792px 0'},
		{file : '34.gif', name : '晕', code : '/:,@@', position : '-816px 0'},
		{file : '35.gif', name : '折磨', code : '/::8', position : '-840px 0'},
		{file : '36.gif', name : '衰', code : '/:,@!', position : '-864px 0'},
		{file : '37.gif', name : '骷髅', code : '/:!!!', position : '-888px 0'},
		{file : '38.gif', name : '敲打', code : '/:xx', position : '-912px 0'},
		{file : '39.gif', name : '再见', code : '/:bye', position : '-936px 0'},
		{file : '40.gif', name : '擦汗', code : '/:wipe', position : '-960px 0'},
		{file : '41.gif', name : '抠鼻', code : '/:dig', position : '-984px 0'},
		{file : '42.gif', name : '鼓掌', code : '/:handclap', position : '-1008px 0'},
		{file : '43.gif', name : '糗大了', code : '/:&-(', position : '-1032px 0'},
		{file : '44.gif', name : '坏笑', code : '/:B-)', position : '-1056px 0'},
		{file : '45.gif', name : '左哼哼', code : '/:<@', position : '-1080px 0'},
		{file : '46.gif', name : '右哼哼', code : '/:@>', position : '-1104px 0'},
		{file : '47.gif', name : '哈欠', code : '/::-O', position : '-1128px 0'},
		{file : '48.gif', name : '鄙视', code : '/:>-|', position : '-1152px 0'},
		{file : '49.gif', name : '委屈', code : '/:P-(', position : '-1176px 0'},
		{file : '50.gif', name : '快哭了', code : '/::\'|', position : '-1200px 0'},
		{file : '51.gif', name : '阴险', code : '/:X-)', position : '-1224px 0'},
		{file : '52.gif', name : '亲亲', code : '/::*', position : '-1248px 0'},
		{file : '53.gif', name : '吓', code : '/:@x', position : '-1272px 0'},
		{file : '54.gif', name : '可怜', code : '/:8*', position : '-1296px 0'},
		{file : '55.gif', name : '菜刀', code : '/:pd', position : '-1320px 0'},
		{file : '56.gif', name : '西瓜', code : '/:<W>', position : '-1344px 0'},
		{file : '57.gif', name : '啤酒', code : '/:beer', position : '-1368px 0'},
		{file : '58.gif', name : '篮球', code : '/:basketb', position : '-1392px 0'},
		{file : '59.gif', name : '乒乓', code : '/:oo', position : '-1416px 0'},
		{file : '60.gif', name : '咖啡', code : '/:coffee', position : '-1440px 0'},
		{file : '61.gif', name : '饭', code : '/:eat', position : '-1464px 0'},
		{file : '62.gif', name : '猪头', code : '/:pig', position : '-1488px 0'},
		{file : '63.gif', name : '玫瑰', code : '/:rose', position : '-1512px 0'},
		{file : '64.gif', name : '凋谢', code : '/:fade', position : '-1536px 0'},
		{file : '65.gif', name : '示爱', code : '/:showlove', position : '-1560px 0'},
		{file : '66.gif', name : '爱心', code : '/:heart', position : '-1584px 0'},
		{file : '67.gif', name : '心碎', code : '/:break', position : '-1608px 0'},
		{file : '68.gif', name : '蛋糕', code : '/:cake', position : '-1632px 0'},
		{file : '69.gif', name : '闪电', code : '/:li', position : '-1656px 0'},
		{file : '70.gif', name : '炸弹', code : '/:bome', position : '-1680px 0'},
		{file : '71.gif', name : '刀', code : '/:kn', position : '-1704px 0'},
		{file : '72.gif', name : '足球', code : '/:footb', position : '-1728px 0'},
		{file : '73.gif', name : '瓢虫', code : '/:ladybug', position : '-1752px 0'},
		{file : '74.gif', name : '便便', code : '/:shit', position : '-1776px 0'},
		{file : '75.gif', name : '月亮', code : '/:moon', position : '-1800px 0'},
		{file : '76.gif', name : '太阳', code : '/:sun', position : '-1824px 0'},
		{file : '77.gif', name : '礼物', code : '/:gift', position : '-1848px 0'},
		{file : '78.gif', name : '拥抱', code : '/:hug', position : '-1872px 0'},
		{file : '79.gif', name : '强', code : '/:strong', position : '-1896px 0'},
		{file : '80.gif', name : '弱', code : '/:weak', position : '-1920px 0'},
		{file : '81.gif', name : '握手', code : '/:share', position : '-1944px 0'},
		{file : '82.gif', name : '胜利', code : '/:v', position : '-1968px 0'},
		{file : '83.gif', name : '抱拳', code : '/:@)', position : '-1992px 0'},
		{file : '84.gif', name : '勾引', code : '/:jj', position : '-2016px 0'},
		{file : '85.gif', name : '拳头', code : '/:@@', position : '-2040px 0'},
		{file : '86.gif', name : '差劲', code : '/:bad', position : '-2064px 0'},
		{file : '87.gif', name : '爱你', code : '/:lvu', position : '-2088px 0'},
		{file : '88.gif', name : 'NO', code : '/:no', position : '-2112px 0'},
		{file : '89.gif', name : 'OK', code : '/:ok', position : '-2136px 0'},
		{file : '90.gif', name : '爱情', code : '/:love', position : '-2160px 0'},
		{file : '91.gif', name : '飞吻', code : '/:<L>', position : '-2184px 0'},
		{file : '92.gif', name : '跳跳', code : '/:jump', position : '-2208px 0'},
		{file : '93.gif', name : '发抖', code : '/:shake', position : '-2232px 0'},
		{file : '94.gif', name : '怄火', code : '/:<O>', position : '-2256px 0'},
		{file : '95.gif', name : '转圈', code : '/:circle', position : '-2280px 0'},
		{file : '96.gif', name : '磕头', code : '/:kotow', position : '-2304px 0'},
		{file : '97.gif', name : '回头', code : '/:turn', position : '-2328px 0'},
		{file : '98.gif', name : '跳绳', code : '/:skip', position : '-2352px 0'},
		{file : '99.gif', name : '挥手', code : '/:oY', position : '-2376px 0'},
		{file : '100.gif', name : '激动', code : '/激动', position : '-2400px 0'},
		{file : '101.gif', name : '街舞', code : '/街舞', position : '-2424px 0'},
		{file : '102.gif', name : '献吻', code : '/献吻', position : '-2448px 0'},
		{file : '103.gif', name : '左太极', code : '/左太极', position : '-2472px 0'},
		{file : '104.gif', name : '右太极', code : '/右太极', position : '-2496px 0'}
	];
	for (var i = 0, l= emotions.length; i < l; i++) {
		emotions[i].codeCN = '[' + emotions[i].name + ']';
		emotions[i].regexp = new RegExp('\\[' + emotions[i].name + '\\]', 'g');
	};
	
	window.PublicWechatEmotions = emotions;
})();


// 编辑工具栏
(function(window){
	var editor = function(id, opt){
		this.defaults = {
			tool : ['emotion'],
			emotionPath : ''
		};
		this.dom = {};
		this.emotions = PublicWechatEmotions;

		this.init(id, opt);
	};
	editor.data = function(dom,d){
		return $(dom).attr("data-"+d)||"";
	};
	editor.prototype.init = function(id, opt){

		this.setting = $.extend({}, this.defaults, opt);

		this.dom.textarea = $('#' + id);
		this.dom.tool = $('<div></div>', {
			'class' : 'wechatEditorTool clearfix'
		});
		this.dom.emotion = $('<div></div>', {
			'class' : 'col emotion',
			'html' : '<a class="title hidetext" rel="emotion" href="javascript://">插入表情</a>'
		});
		this.dom.preview = $('<a></a>', {
			'class' : 'col preview',
			'rel' : 'preview',
			'href' : 'javascript://',
			'html' : '预览'
		});
		this.dom.emotionTmp = '<ul class="list clearfix" onselectstart="return false;">'
			+	'<% for (i = 0; i < list.length; i++) { %>'
			+	'<li class="item" title="<%=list[i].name%>" data-pic="' + this.setting.emotionPath + '<%=list[i].file%>" data-name="<%=list[i].name%>" data-code="<%=list[i].codeCN%>"><i style="background-position:<%=list[i].position%>;"></i></li>'
			+	'<% } %>'
			+	'</ul>';

		this.dom.tool.append(this.dom.emotion)
			.insertAfter(this.dom.textarea);

		this.buildEmotion();
		this.bindEvents();
	};
	
	// 获取光标位置
	editor.prototype.getCursorPosition = function(){
		var textarea = this.dom.textarea[0];
		var CaretPos = 0;

		// IE Support
		if (document.selection) {
			textarea.focus();

			var Sel = document.selection.createRange();
			var Sel2 = Sel.duplicate();

			Sel2.moveToElementText(textarea);

			var CaretPos = -1;

			while (Sel2.inRange(Sel)) {
				Sel2.moveStart('character');
				CaretPos++;
			};

		// Firefox support
		} else if (textarea.selectionStart || textarea.selectionStart == '0') {
			CaretPos = textarea.selectionStart;
		};

		return (CaretPos);
	};

	// 设置光标位置函数
	editor.prototype.setCursorPosition = function(n){
		var textarea = this.dom.textarea[0];

		if(textarea.setSelectionRange){
			textarea.focus();
			textarea.setSelectionRange(n, n);

		} else if (textarea.createTextRange) {
			var range = textarea.createTextRange();
			range.collapse(true);
			range.moveEnd('character', n);
			range.moveStart('character', n);
			range.select();
		};
	};

	// 构建表情
	editor.prototype.buildEmotion = function(){
		var _this = this;
		var _html='<ul class="list clearfix" onselectstart="return false;">';
		for (var i = 0; i < this.emotions.length; i++) {
			_html+='<li class="item" title="' + this.emotions[i].name +'" data-pic="' + this.setting.emotionPath + this.emotions[i].file +'" data-name="' + this.emotions[i].name +'" data-code="' + this.emotions[i].codeCN +'"><i style="background-position:' + this.emotions[i].position +';"></i></li>';
		}
		_html+='</ul>';
		_this.dom.emotionList = $('<div></div>', {'class' : 'wechatEditorEmotion'});
		_this.dom.emotionPreview = $('<span></span>', {'class' : 'preview'});

		_this.dom.emotionList.html(_html)
			.append(_this.dom.emotionPreview)
			.appendTo(_this.dom.emotion)
			.on('mouseenter', 'li', function(){
				var _html = '<img src="' + $(this).attr('data-pic') + '" alt="' + $(this).attr('data-name') + '">';
				_this.dom.emotionPreview.html(_html);
			})
			.on('click', 'li', function(){
				var _val = _this.dom.textarea.val();
				var _key = $(this).attr('data-code');
				var _newval;

				if (typeof _this.textAreaPos === 'number' && _this.textAreaPos < _val.length) {
					_newval = _val.slice(0, _this.textAreaPos);
					_newval += _key;
					_newval += _val.slice(_this.textAreaPos);
				}else{
					_this.textAreaPos = _val.length;
					_newval = _val + _key;
				};
				
				_this.dom.textarea.val(_newval);
				_this.setCursorPosition(_this.textAreaPos + _key.length);
				_this.hideEmotion();
			});
	};

	// 显示表情列表
	editor.prototype.showEmotion = function(){
		this.toggleEmotion('show');
	};

	// 隐藏表情列表
	editor.prototype.hideEmotion = function(){
		this.toggleEmotion('hide');
	};

	// 显示或隐藏表情列表
	editor.prototype.toggleEmotion = function(s){
		if (s === 'show') {
			this.dom.emotionList.show();
		} else if (s === 'hide') {
			this.dom.emotionList.hide();
		} else {
			this.dom.emotionList.toggle();
		};
	};
	
	// 预览信息
	editor.prototype.previewMessage = function(){
		if (!this.dom.textarea.val().length) {
			this.dom.textarea.trigger('focus');
			return false;
		};
		
		var _this = this;
		var _emotions = this.emotions;
		var _info = WeiWeb.replaceEnter($.trim(this.dom.textarea.val()));

		for (var i = 0, l= _emotions.length; i < l; i++) {
			_info = _info.replace(_emotions[i].regexp, '<img src="' + _this.setting.emotionPath + _emotions[i].file + '">');
		};

		_info = '<div class="wechatPreviewPop">' + _info + '</div>';

		cxDialog({
			lock : true,
			width : 280,
			info : _info
		});
	};

	// 绑定事件
	editor.prototype.bindEvents = function(){
		var _this = this;
		
		_this.dom.textarea.on('focus', function(){
			_this.textAreaPos = _this.hideEmotion();
		});
		
		_this.dom.textarea.on('blur', function(){
			_this.textAreaPos = _this.getCursorPosition();
		});
		
		_this.dom.tool.on('click', 'a', function(){
			var _rel = this.rel;
			
			if (_rel === 'emotion') {
				_this.toggleEmotion();

			} else if (_rel === 'preview') {
				_this.previewMessage();

			};
		});
	};
	
	window.wechatEditor = editor;
	
})(window);