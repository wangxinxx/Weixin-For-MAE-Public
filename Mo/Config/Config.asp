﻿<%
Dim MO_ROOT,MO_APP, MO_CORE, MO_TEMPLATE_NAME, MO_TEMPLATE_SPLIT, MO_TEMPLATE_PERX,MO_TABLE_PERX,MO_CACHE_DIR,MO_CACHE,MO_LIB_CACHE,MO_REWRITE_MODE,MO_APP_NAME,MO_APP_ENTRY,MO_REWRITE_CONF,MO_MODEL_CACHE
Dim MO_DEBUG,MO_COMPILE_CACHE,MO_CHARSET,MO_METHOD_CHAR,MO_ACTION_CHAR,MO_DATABASE_PATH,MO_PRE_LIB,MO_TAG_LIB,MO_END_LIB,MO_DIRECT_OUTPUT,MO_COMPILE_CACHE_EXPIRED,MO_COMPILE_STRICT,MO_PREETY_HTML
Dim MO_IMPORT_COMMON_FILES,MO_SESSION_WITH_SINGLE_TAG,DISABLED_FUNCTIONS,DISABLED_MODELS,MO_GROUP_CHAR,MO_LANGUAGE
MO_ROOT = ""
MO_APP_NAME = "App"
MO_APP = "App/"
MO_CORE = "Mo/"
MO_CACHE= false
MO_CACHE_DIR = ""
MO_TEMPLATE_NAME = "default"
MO_TEMPLATE_SPLIT = "/"
MO_TEMPLATE_PERX = "html"
MO_TABLE_PERX = "Mo_"
MO_DEBUG= true
MO_SHOW_SERVER_ERROR= true
MO_COMPILE_CACHE= true
MO_COMPILE_CACHE_EXPIRED = 0
MO_COMPILE_STRICT = false
MO_CHARSET = "UTF-8"
MO_METHOD_CHAR = "m"
MO_ACTION_CHAR = "a"
MO_GROUP_CHAR = "g"
MO_PRE_LIB = ""
MO_TAG_LIB = ""
MO_END_LIB = ""
MO_DIRECT_OUTPUT = false
MO_LIB_CACHE = false
MO_REWRITE_MODE = ""
MO_REWRITE_CONF = ""
MO_PREETY_HTML = false
MO_APP_ENTRY = ""
MO_MODEL_CACHE = false
MO_IMPORT_COMMON_FILES = ""
MO_SESSION_WITH_SINGLE_TAG = false
DISABLED_FUNCTIONS="F.execute,F.include,F.import,F.executeglobal,F.require,executeglobal,execute,include,eval,IncludeFile,LoadVBScript,LoadScript,LoadFile,SaveFile"
DISABLED_MODELS = ""
%>