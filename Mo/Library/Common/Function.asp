﻿<%
'****************************************************
'@DESCRIPTION:	fetch a paged string of parms
'@PARAM:	URL [String] : if URL is blank,we will fetch the URL automatic,if not,you must give a '{#page}' replacement
'@PARAM:	RecordCount [Int] : record count of data
'@PARAM:	PageSize [Int] : record count per page
'@PARAM:	CurrentPage [Int] : current page index
'@RETURN:	[String] paged string
'****************************************************
public function CreatePageList(Byval URL,ByVal RecordCount, ByVal PageSize, ByVal CurrentPage)
	Dim PageCount ,PageStr, I
	if URL="" then
	URL="?" & replaceEx(request.QueryString & "&page={#page}","(\&)?page\=(\d+)","")
	URL = replace(URL,"?&","?")
	end if
	CurrentPage = int(CurrentPage)
	RecordCount = int(RecordCount)
	PageSize = int(PageSize)
	If RecordCount Mod PageSize =0 Then
	Pagecount = RecordCount / PageSize
	Else
	Pagecount = Int(RecordCount / PageSize) + 1
	End If
	if Pagecount<=1 then exit function
	PageStr = "共[" & RecordCount & "]条记录 [" & PageSize & "]条/页 当前[" & CurrentPage & "/" & PageCount & "]页&nbsp; "
	If CurrentPage = 1 Or PageCount = 0 Then
	PageStr = PageStr & "首页&nbsp;"
	PageStr = PageStr & "上页&nbsp;"
	Else
	PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", 1) & """>首页</a>&nbsp;"
	PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", CurrentPage-1) & """>上页</a>&nbsp;"
	End If
	If CurrentPage = Pagecount  Or PageCount = 0 Then
	PageStr = PageStr & "下页&nbsp;"
	PageStr = PageStr & "尾页&nbsp;"
	Else
	PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", CurrentPage + 1) & """>下页</a>&nbsp;"
	PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", Pagecount) & """>尾页</a>&nbsp;"
	End If
	CreatePageList = PageStr
end function

function WriteStreamText(byref stream, byval txt)
	if isnull(txt) then txt="" else txt = cstr(txt)
	stream.WriteText txt
end function
%>