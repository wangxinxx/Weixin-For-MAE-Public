/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
/**
 * ISO/IEC 9797-1 Padding Method 2.
 */
exports.pad.Iso97971 = {
    pad: function (data, blockSize) {
        // Add 0x80 byte
        data.concat(exports.lib.WordArray.create([0x80000000], 1));

        // Zero pad the rest
        exports.pad.ZeroPadding.pad(data, blockSize);
    },

    unpad: function (data) {
        // Remove zero padding
        exports.pad.ZeroPadding.unpad(data);

        // Remove one more byte -- the 0x80 byte
        data.sigBytes--;
    }
};
