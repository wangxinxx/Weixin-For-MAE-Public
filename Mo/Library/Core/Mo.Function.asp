<%
function VBSExecute(cmd)
	Execute cmd
end function
'****************************************************
'@DESCRIPTION:	get weekday of ome month's first day
'@PARAM:	srcDate [Date] : the date need to get
'@RETURN:	[Int] weeday(first day is monday)
'****************************************************
function GetWeekdayOfMonthFirstDay(srcDate)
	GetWeekdayOfMonthFirstDay = weekday(dateadd("d",1-day(srcDate),srcDate),2)
end function

'****************************************************
'@DESCRIPTION:	get day of ome month's first mondy
'@PARAM:	srcDate [Date] : the date need to get
'@RETURN:	[Int] day
'****************************************************
function GetDayOfFirstWeekday(srcDate)
	GetDayOfFirstWeekday = 9 - weekday(dateadd("d",1-day(srcDate),srcDate),2)
	if GetDayOfFirstWeekday>7 then GetDayOfFirstWeekday=GetDayOfFirstWeekday-7
end function

'****************************************************
'@DESCRIPTION:	get a complete GUID string
'@RETURN:	[String] GUID String
'****************************************************
function GetGUID()
    GetGUID = F.guid("b")
end function

'****************************************************
'@DESCRIPTION:	get a GUID string without "-,{,}"
'@RETURN:	[String] GUID String
'****************************************************
function GetGUIDString()
    GetGUID = F.guid("n")
end function


'****************************************************
'@DESCRIPTION:	include an UTF-8 vbscript file,you can use the classes and variables in the file
'@PARAM:	path [String] : the source file
'****************************************************
public sub Include(byval path)
	IncludeFile path,"utf-8"
end sub

'****************************************************
'@DESCRIPTION:	include an vbscript file,you can use the classes ,functions and variables in the file
'@PARAM:	path [String] : the source file
'@PARAM:	charset [String] : file text encoding
'****************************************************
public sub IncludeFile(byval path,byval charset)
	dim ret,filesum,content
	filesum = MoLibMD5(path)
	content = Application(MO_APP_NAME & "_lib_" & filesum & "_cache")
	if content="" or (not MO_LIB_CACHE) then
		ret = LoadScript(path,charset,content)
		if ret>0 then
			Application.Lock()
			Application(MO_APP_NAME & "_lib_" & filesum & "_cache") =content
			Application(MO_APP_NAME & "_lib_" & filesum & "_cache_lng") =ret
			Application.UnLock() 
		end if
	end if
	if content<>"" then
		if Application(MO_APP_NAME & "_lib_" & filesum & "_cache_lng")=1 then ExecuteGlobal content
		if Application(MO_APP_NAME & "_lib_" & filesum & "_cache_lng")=2 then F.execute content
	end if
end sub
'****************************************************
'@DESCRIPTION:	if a variable is defined
'@PARAM:	var [Variant] : source variable
'@RETURN:	[Boolean] return true if the variable is defined,else return false
'****************************************************
function Defined(var)
	Defined = (vartype(var)<>0)
end function

'****************************************************
'@DESCRIPTION:	if a function/sub is defined
'@PARAM:	name [String] : the name of a function/sub
'@RETURN:	[Boolean] return true if the function/sub is defined,else return false
'****************************************************
function Function_Exists(name)
	on error resume next
	Function_Exists = false
	if typename(GetRef(name))="Object" then Function_Exists = true
	if err then
	Function_Exists = false
	err.clear
	end if
end function

'****************************************************
'@DESCRIPTION:	if a variable is empty
'@PARAM:	var [Variant] : source variable
'@RETURN:	[Boolean] return true if the variable is empty,else return false
'****************************************************
function Is_Empty(var)
	Is_Empty = false
	if not isobject(var) then
	if not defined(var) then
		Is_Empty=true
	elseif isempty(var) then
		Is_Empty=true
	elseif isnull(var) then
		Is_Empty=true
	elseif var="" then
		Is_Empty=true
	end if
	else
	if var is nothing then Is_Empty=true
	end if
end function

'****************************************************
'@DESCRIPTION:	get random string
'@PARAM:	strSeed [String] : random seed
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[String] random string
'****************************************************
function Rnd_(strSeed,intLength)
    Dim seedLength, pos, Strs, i
    seedLength = Len(strSeed)
    Strs = ""
    For i = 1 To intLength
	randomize timer*1000
        Strs = Strs & Mid(strSeed, Int(seedLength * Rnd) + 1, 1)
    Next
    Rnd_ = Strs
end function

'****************************************************
'@DESCRIPTION:	get random string with UCase letters(A-Z)
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[Boolean] random string
'****************************************************
function RndUCase(intLength)
    RndUCase = Rnd_("IJKLMNOPQRSTUVWXYZABCDEFGH",intLength)
end function

'****************************************************
'@DESCRIPTION:	get random string with LCase letters(a-z)
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[Boolean] random string
'****************************************************
function RndLCase(intLength)
    RndLCase = Rnd_("abcdefghijklmnopqrstuvwxyz",intLength)
end function

'****************************************************
'@DESCRIPTION:	get random string with letters(a-zA-Z)
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[Boolean] random string
'****************************************************
function RndStr(intLength)
    RndStr = Rnd_("abcdefghiIJKLMNOPQRSTUVWXYZjklmnopqrstuvwxyzABCDEFGH",intLength)
end function

'****************************************************
'@DESCRIPTION:	get random string with numbers
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[Boolean] random string
'****************************************************
function RndNumber(intLength)
    RndNumber = Rnd_("12345678906789012678901234534567890126789012345345",intLength)
end function

'****************************************************
'@DESCRIPTION:	get random string with numbers and letters
'@PARAM:	intLength [Int] : random string length
'@RETURN:	[Boolean] random string
'****************************************************
function RndStr1(intLength)
    RndStr1 = Rnd_("abcdefghiIJKLMNOPQRSTUVWXYZjklmnopqrstuvwxyzABCDEFGH12345678906789012678901234534567890126789012345345",intLength)
end function

'****************************************************
'@DESCRIPTION:	Test a string with RegExp
'@PARAM:	Str [String] : source string
'@PARAM:	RegStr [String] : source RegExp string
'@RETURN:	[Boolean] return true,if Str is matched with RegStr,else return false
'****************************************************
function RegTest(Str,RegStr)
	if isnull(Str) then Str=""
	Dim Reg, Mag
	Set Reg = New RegExp
	With Reg
	.IgnoreCase = True
	.Global = True
	.Pattern = RegStr
	RegTest = .test((str))
	End With
	Set Reg = nothing
end function

'****************************************************
'@DESCRIPTION:	Replace a string with RegExp as str
'@PARAM:	sourcestr [String] : source string
'@PARAM:	regString [String] : RegExp string need to find
'@PARAM:	str [String] : string need to replace with
'@RETURN:	[String] return replaced string 
'****************************************************
function ReplaceEx(sourcestr, regString, str)
    dim re
	Set re = new RegExp
	re.IgnoreCase = true
	re.Global = True
	re.pattern = "" & regString & ""
	str = re.replace(sourcestr, str)
    set re = Nothing
    ReplaceEx = str
end function

'****************************************************
'@DESCRIPTION:	get matched collection of RegExp
'@PARAM:	Str [String] : source string need to match
'@PARAM:	Rex [String] : RegExp string need to find
'@RETURN:	[Collection] return a collection of matches.it can be enum and has a 'count' property
'****************************************************
function GetMatch(ByVal Str, ByVal Rex)
	Dim Reg
	Set Reg = New RegExp
	With Reg
	.IgnoreCase = True
	.Global = True
	.Pattern = Rex
	Set GetMatch = .Execute((str))
	End With
	Set Reg = nothing
end function

'****************************************************
'@DESCRIPTION:	load vbscript file into memory
'@PARAM:	filename [String] : file need to load
'@PARAM:	charset [String] : the text charset of file
'@RETURN:	[string] the text of the file,we have removed script tags from start and end.
'****************************************************
public function LoadVBScript(byval filename,byval charset)
	dim ret
	ret = LoadFile(filename,charset)
	ret = replaceex(ret,"(^(\s+)|(\s+)$)","")
	ret = trim(ret)
	if left(ret,2)="<%" then ret = mid(ret,3)
	if right(ret,2)=replace("% >"," ","") then ret = left(ret,len(ret)-2)
	LoadVBScript = ret
end function

'****************************************************
'@DESCRIPTION:	load vbscript/jscript file into memory
'@PARAM:	filename [String] : file need to load
'@PARAM:	charset [String] : the text charset of file
'@PARAM:	content [String] : file content
'@RETURN:	[Int] 0 for load failed, 1 for vbscript, 2 for jscript.
'****************************************************
public function LoadScript(byval filename,byval charset,byref content)
	dim ret
	ret = LoadFile(filename,charset)
	ret = ReplaceEx(ret,"(^(\s+)|(\s+)$)","")
	ret = trim(ret)
	if ret="" then
		LoadScript=0
		exit function
	end if
	if left(ret,2)="<%" then
		ret = mid(ret,3)
		if right(ret,2)=replace("% >"," ","") then ret = left(ret,len(ret)-2)
		content = ret
		LoadScript = 1
	else
		ret = ReplaceEx(ret,"((^(\s*)<script([\s\S]+?)>)|(</script>(\s*)$))","")
		content = ret
		if ret<>"" then LoadScript = 2 else LoadScript=0
	end if
end function

'****************************************************
'@DESCRIPTION:	read a text file
'@PARAM:	path [String] : file need to load
'@PARAM:	charset [String] : the text charset of file
'@RETURN:	[string] the complete text of the file
'****************************************************
public function LoadFile(path,charset)
	if not F.fso.FileExists(path) then
		LoadFile = ""
		exit function
	end if
	dim stream
	set stream = F.stream()
	stream.mode = 3
	stream.type=2
	stream.charset=charset
	stream.open
	stream.loadfromfile path
	LoadFile = stream.readtext()
	stream.close
	set stream = nothing
end function

'****************************************************
'@DESCRIPTION:	write text to file
'@PARAM:	path [String] : file need to write
'@PARAM:	content [String] : content need to write
'@PARAM:	charset [String] : the text charset of file
'****************************************************
public function SaveFile(path,content,charset)
	dim stream
	set stream = F.stream()
	stream.mode = 3
	stream.type=2
	stream.charset=charset
	stream.open
	stream.writetext(content)
	stream.savetofile path,2
	stream.close
	set stream = nothing
end function

'****************************************************
'@DESCRIPTION:	convert variable to date
'@PARAM:	sDateTime [Variant] : variable need to be converted
'@RETURN:	[Date] if convert successfully,return the date, or return null
'****************************************************
function ToDate(sDateTime)
	if isdate(sDateTime) then ToDate = cdate(sDateTime) else ToDate=null
end function

'****************************************************
'@DESCRIPTION:	format datetime to string
'@PARAM:	sDateTime [Date] : datetime need to be formated
'@PARAM:	sReallyDo [String] : format string, Eg. "yyyy-MM-dd","yyyy-MM-dd HH:mm:ss"
'@RETURN:	[string] formated datetime string
'****************************************************
function FormatDate(sDateTime, sReallyDo)
	if isdate(sDateTime) then sDateTime = cdate(sDateTime)
    FormatDate = F.FormatDate(sDateTime, sReallyDo)
end function

'****************************************************
'@DESCRIPTION:	insert an item into source array at start
'@PARAM:	Ary [Array] : source array
'@PARAM:	item [Variant] : item need to be inserted
'****************************************************
function UnShift(Byref Ary,item)
	redim preserve Ary(ubound(Ary)+1) 
	dim i
	for i=ubound(Ary) to 1 step -1
	if typename(Ary(i-1))="JScriptTypeInfo" then
		Set Ary(i) = Ary(i-1)
	else
		Ary(i) = Ary(i-1)
	end if
	next
	if typename(item)="JScriptTypeInfo" then
	set Ary(0) = item
	else
	Ary(0) = item
	end if
end function

'****************************************************
'@DESCRIPTION:	insert an item into source array at last
'@PARAM:	Ary [Array] : source array
'@PARAM:	item [Variant] : item need to be inserted
'****************************************************
function Push(Byref Ary,item)
	redim preserve Ary(ubound(Ary)+1) 
	if typename(item)="JScriptTypeInfo" then
	set Ary(ubound(Ary)) = item
	else
	Ary(ubound(Ary)) = item
	end if
end function

'****************************************************
'@DESCRIPTION:	remove the last item from source array
'@PARAM:	Ary [Array] : source array
'****************************************************
function Pop(Byref Ary)
	redim preserve Ary(ubound(Ary)-1) 
end function

'****************************************************
'@DESCRIPTION:	remove the first item from source array
'@PARAM:	Ary [Array] : source array
'****************************************************
function UnPop(Byref Ary)
	dim i
	for i=0 to ubound(Ary)-1
	if typename(Ary(i+1))="JScriptTypeInfo" then
		Set Ary(i) = Ary(i+1)
	else
		Ary(i) = Ary(i+1)
	end if
	next
	redim preserve Ary(ubound(Ary)-1) 
end function

'****************************************************
'@DESCRIPTION:	Join source array with a string(spli), Eg. Response.Write Join(array("a","b","c"),"-")
'@PARAM:	Ary [Array(String)] : source array
'@PARAM:	spli [String] : join string
'@RETURN:	[String] joined string
'****************************************************
function Join(Byref Ary,byval spli)
	dim ret
	for i=0 to ubound(Ary)
	ret = ret & Ary(i) & spli
	next
	Join = left(ret,len(ret)-len(spli))
end function

'****************************************************
'@DESCRIPTION:	copy Ary2 to cover the last items of Ary
'@PARAM:	Ary [Array] : source array
'@PARAM:	Ary2 [Array] : need to copy
'@RETURN:	[Array] new array
'****************************************************
function RightCopy(Ary,Ary2)
	if ubound(Ary2)>=ubound(Ary) then
	RightCopy = Ary2
	else
	dim i,j
	j = ubound(Ary)-ubound(Ary2)
	for i=0 to ubound(Ary2)
		Ary(i+j) = Ary2(i)
	next
	RightCopy = Ary
	end if
end function
%>