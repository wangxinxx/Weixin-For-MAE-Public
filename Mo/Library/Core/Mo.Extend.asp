<script language="jscript" runat="server">
/****************************************************
'@DESCRIPTION:	ExceptionManager
'****************************************************/
var ExceptionManager={
	exceptions:[],
	put:function(exception_){
		if(arguments.length==1){
			ExceptionManager.exceptions.push(exception_);
		}else if(arguments.length==3){
			ExceptionManager.exceptions.push(new Exception(arguments[0],arguments[1],arguments[2]));
		}
	},
	putjsexception:function(exception_,source){
		ExceptionManager.exceptions.push(new Exception(exception_.number & 0xffff,source,exception_.description));
	},
	clear:function(){
		while(ExceptionManager.exceptions.length>0)ExceptionManager.exceptions.pop();
	},
	debug:function(){
		var returnValue="";
		for(var i=0;i<ExceptionManager.exceptions.length;i++){
			var exception_=ExceptionManager.exceptions[i];
			returnValue+=F.format("[0x{0:X8}] {1} : {2}<br />",exception_.Number,exception_.Source,exception_.Description);
		}
		return returnValue;
	}
};
/****************************************************
'@DESCRIPTION:	Exception
'@PARAM:	number [Long] : exception Number
'@PARAM:	source [String] : exception Source
'@PARAM:	message [String] : exception message
'****************************************************/
function Exception(number,source,message){
	this.Number = number||0;
	if(this.Number<0)this.Number = Math.pow(2,32)+this.Number;
	this.Source = source||"";
	this.Message = message||"";
	this.Description = message||"";
}
Exception.New = function(number,source,message){return new Exception(number,source,message);};
/****************************************************
'@DESCRIPTION:	Enumerator for jscript array. you may use it in vbscript almost.
'@PARAM:	obj [Array] : a jscript array
'****************************************************/
function MoEnumerator(obj){
	this.index = -1;
	this.container = obj;
	this.Current=null;
	this.MoveNext=function() {
		this.index++;
		if (this.index < this.container.length){
			this.Current = this.container[this.index];
			return true;
		}
		return false;
	};
	this.Reset=function()
	{
		this.index = -1;
	};
}
/****************************************************
'@DESCRIPTION:	create a new JScript object and return it
'@RETURN:	[Obect]
'****************************************************/
function object(){return new Object();}

/****************************************************
'@DESCRIPTION:	create a list object(like a recordset object),you can use it in vbscript
'@PARAM:	ds [Object] : recordset object
'@PARAM:	pagesize [Int] : recordcount per page
'****************************************************/
function Mo___(ds,pagesize){
	return new __Obj__(ds,pagesize);	
}

/****************************************************
'@DESCRIPTION:	create a list object from jscript array and return it
'@PARAM:	obj [Array] : record array
'@RETURN:	[Object] list
'****************************************************/
Mo___.FromJsArray = function(obj){
	var data = new __Obj__();
	data.FromJson(obj);
	return data;
};

/****************************************************
'@DESCRIPTION:	create a list object(like a recordset object)
'@PARAM:	ds [Object] : recordset object
'@PARAM:	pagesize [Int] : recordcount per page
'****************************************************/
function __Obj__(ds,pagesize){
	this.LIST__=[];
	this.index=-1;
	this.pagesize=pagesize||-1;
	this.recordcount=0;
	this.currentpage=1;
	this.GetEnumerator=function(){
		return new MoEnumerator(this.LIST__);
	};
	if(ds!==undefined)this.AddDataSet(ds,pagesize);
}

/****************************************************
'@DESCRIPTION:	dispose object,release related resources
'****************************************************/
__Obj__.prototype.dispose=function(){
	while(this.LIST__.length>0){
		F.dispose(this.LIST__.pop());
	}
};

/****************************************************
'@DESCRIPTION:	add a json object to list. eg: AddJson({a:1,b:2})
'@PARAM:	obj [Object] : json object.
'@RETURN:	[Variant] description
'****************************************************/
__Obj__.prototype.AddJson=function(obj){
	this.LIST__.push(obj);
	
};

/****************************************************
'@DESCRIPTION:	init the list from jscript array
'@PARAM:	obj [Array] : record array
'****************************************************/
__Obj__.prototype.FromJson=function(obj){
	this.LIST__=obj;
	
};

/****************************************************
'@DESCRIPTION:	get list state.
'@PARAM:	dateformat [String] : datetime format string
'@RETURN:	[String] state string.this state woule be save to local file as model cache
'****************************************************/
__Obj__.prototype.GetState=function(dateformat){
	if(dateformat==undefined) dateformat = "yyyy-MM-dd HH:mm:ss";
	var returnValue="{";
	returnValue+="\"pagesize\":" + this.pagesize +",";
	returnValue+="\"recordcount\":" + this.recordcount +",";
	returnValue+="\"currentpage\":" + this.currentpage +",";
	returnValue+="\"LIST__\":" + this.getjson(dateformat) +"}";
	return returnValue;
};

/****************************************************
'@DESCRIPTION:	init list object from state
'@PARAM:	ObjectState [Object(json)] : json object from saved state. the saved state would load from local model cache
'****************************************************/
__Obj__.prototype.FromState=function(ObjectState){
	this.LIST__=ObjectState.LIST__;
	this.pagesize = ObjectState.pagesize;
	this.recordcount = ObjectState.recordcount;
	this.currentpage = ObjectState.currentpage;
};

/****************************************************
'@DESCRIPTION:	init list from json string
'@PARAM:	obj [String] : json string.it can be parse as jscript array
'****************************************************/
__Obj__.prototype.AddJsonString=function(obj){
	var json = F.json(obj);
	if(json!=null)this.LIST__.push(json);
	
};

/****************************************************
'@DESCRIPTION:	Add a record of recordset to list
'@PARAM:	rs [recordset] : recordset
'****************************************************/
__Obj__.prototype.Add=function(rs){
	var tmp__=new Object();
	for(var i=0;i<rs.fields.count;i++){
		tmp__[rs.fields(i).Name]=rs.fields(i).value;
	}
	this.LIST__.push(tmp__);
	
};
/****************************************************
'@DESCRIPTION:	add dataset to list
'@PARAM:	rs [recordset] : recordset
'@PARAM:	pagesize [Int] : recordcount per page.if pagesize is -1 or blank,all the records will be add to list
'****************************************************/
__Obj__.prototype.AddDataSet=function(rs,pagesize){
	if(rs==null){ExceptionManager.put(new Exception(0,"__Obj__.AddDataSet(rs,pagesize)","Recordset is null"));return;}
	if(rs.state==0){ExceptionManager.put(new Exception(0,"__Obj__.AddDataSet(rs,pagesize)","Recordset's state is 'closed'."));return;}
	try{
		if(pagesize==undefined)pagesize=-1;
		var ps = rs.AbsolutePosition;
		var k=0;
		while(!rs.eof && (k<pagesize || pagesize==-1)){
			k++;
			var tmp__=new Object();
			for(var i=0;i<rs.fields.count;i++){
				tmp__[rs.fields(i).Name]=rs.fields(i).value;
			}
			this.LIST__.push(tmp__);
			rs.MoveNext();
		}
		try{
			rs.AbsolutePosition=ps;
		}catch(ex){}
	}catch(ex){
		ExceptionManager.put(new Exception(ex.number,"__Obj__.AddDataSet(rs,pagesize)",ex.description));
	}
};
/****************************************************
'@DESCRIPTION:	add a new record
'@RETURN:	[Int] record index in the list
'****************************************************/
__Obj__.prototype.AddNew=function(){
	this.LIST__.push(new Object());
	return this.LIST__.length-1;
};

/****************************************************
'@DESCRIPTION:	Set field of a record
'@PARAM:	key [String] : field name
'@PARAM:	value [Variant] : field value
'@PARAM:	index [Int] : record index. if record index is blank,current record will be the last item of list
'****************************************************/
__Obj__.prototype.Set=function(key,value,index){
	if(index==undefined)index=this.LIST__.length-1;
	if(index<0 || index> this.LIST__.length-1)return;
	this.LIST__[index][key]=value;
};
/****************************************************
'@DESCRIPTION:	remove enum index to start
'****************************************************/
__Obj__.prototype.Reset = function(){
	this.index=-1;
};
/****************************************************
'@DESCRIPTION:	if the enum is at end
'@RETURN:	[Boolean] if the enum is at end return true,or return false
'****************************************************/
__Obj__.prototype.Eof = function(){
	return this['LIST__'].length==0 || this.index+1>=this['LIST__'].length;	
};

/****************************************************
'@DESCRIPTION:	Read the next record
'@PARAM:	name [String] : field name.
'@RETURN:	[Variant] if field name is blank,the method will return the record,or return field value.
'****************************************************/
__Obj__.prototype.Read = function(name){
	name = name ||"";
	this.index++;
	if(name=="" || name==undefined){
		return this['LIST__'][this.index];
	}else{
		return this['LIST__'][this.index].getter__(name);
	}
};

/****************************************************
'@DESCRIPTION:	assign self to system. you can use loop tag in template to display this object.
'@PARAM:	name [String] : variable name
'@RETURN:	[Object(list)] self
'****************************************************/
__Obj__.prototype.assign=function(name){
	Mo.Assign(name,this);
	return this;
};

/****************************************************
'@DESCRIPTION:	get json data of self
'@PARAM:	dateformat [String] : datetime format string
'@RETURN:	[String] json string
'****************************************************/
__Obj__.prototype.getjson=function(dateformat){
	var ret="[";
	//dateformat = dateformat || "yyyy-MM-dd HH:mm:ss";
	while(!this.Eof()){
		var D = this.Read();	
		ret+="{"
		for(var i in D){
			if(!D.hasOwnProperty(i))continue;
			var val = D.getter__(i)
			var ty = typeof val;
			ret+="\"" + i + "\":";
			if(ty=="number"){
				ret+=val+",";
			}else if(ty=="date"){
				if(dateformat===undefined)ret+="\"" + (new Date(val)).getTime() + "\",";
				else ret+="\"" + F.formatdate(val,dateformat) + "\",";
			}else if(ty=="string"){
				ret+="\"" + F.jsEncode(val) + "\",";
			}else{
				if(!isNaN(val)){
					ret+=val+",";
				}else{
					ret+="\"" +val + "\",";
				}
			}
		}
		if(ret.substr(ret.length-1,1)==",")ret = ret.substr(0,ret.length-1);
		ret+="},";
	}
	if(ret.substr(ret.length-1,1)==",")ret = ret.substr(0,ret.length-1);
	ret+="]";
	return ret;
};

/****************************************************
'@DESCRIPTION:	create record object. you can use it in vbscript.
'****************************************************/
function Record__(){
	return new __Record__(arguments);	
}

/****************************************************
'@DESCRIPTION:	create record object
'@PARAM:	args [arguments] : init data(arguments length = 2 * x). eg: var record = new __Record__("name","anlige","age",23);
'****************************************************/
function __Record__(args){
	this.table={};
	this.pk="";
	if(args.length % 2==0){
		for(var i=0;i<args.length-1;i+=2){
			this.set(args[i],args[i+1]);
		}
	}
}

/****************************************************
'@DESCRIPTION:	init data from post data
'@PARAM:	pk [String] : primary key of table
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.frompost=function(pk){
	pk = pk ||"";
	F.post();
	for(var i in F.post__){
		if(i.length>2 && i.substr(i.length-2)==":i")continue;
		if(!F.post__.hasOwnProperty(i))continue;
		if(i.toLowerCase()!=pk.toLowerCase()){
			this.set(i,F.post__[i]);
		}else{
			this.pk = F.post__[i];
		}
	}
	return this;
};

/****************************************************
'@DESCRIPTION:	set field value
'@PARAM:	name [String] : field name
'@PARAM:	value [Variant] : field value
'@PARAM:	type [Variant] : field type. it can be blank
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.set=function(name,value,type){
	type=type||"string";
	delete this.table[name];
	this.table[name]={"value":value,"type":type}
	return this;
};

/****************************************************
'@DESCRIPTION:	get field value
'@PARAM:	name [String] : field name
'@RETURN:	[Variant] field value
'****************************************************/
__Record__.prototype.get=function(name){
	if(this.table[name]!==undefined)return this.table[name].value;
	return "";
};

/****************************************************
'@DESCRIPTION:	remove field
'@PARAM:	[names] [arguments] : field name. Eg: remove("name","age")
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.remove=function(){
	for(var i=0;i<arguments.length;i++){
		delete this.table[arguments[i]];
	}
	return this;
};

/****************************************************
'@DESCRIPTION:	clear all fields
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.clear=function(){
	delete this.table;
	this.table={};
	return this;
};

/****************************************************
'@DESCRIPTION:	assign record to system
'@PARAM:	name [String] : variable name
'@RETURN:	[Object] return self
'****************************************************/
__Record__.prototype.assign=function(name){
	var obj = {};
	for(var i in this.table){
		if(!this.table.hasOwnProperty(i))continue;
		obj[i]=	this.table[i].value;
	}
	Mo.assign(name,obj);
	return this;
};
/****************************************************
'@DESCRIPTION:	if the property is defined in the Object
'@PARAM:	key [String] : property name
'@RETURN:	[Boolean] if the property is defined return true,or return false
'****************************************************/
Object.prototype.isset__=function(key){
	return this.hasOwnProperty(key);
};

/****************************************************
'@DESCRIPTION:	get the property value
'@PARAM:	key [String] : property name
'@RETURN:	[Variant] property value
'****************************************************/
Object.prototype.getter__=function(key){
	if(key==undefined)key="";
	return this.hasOwnProperty(key)? (this[key]===null?"":this[key]):"";
};
/****************************************************
'@DESCRIPTION:	set the property value
'@PARAM:	key [String] : property name
'@PARAM:	value [Variant] : property value
'****************************************************/
Object.prototype.setter__=function(key,value){
	this[key]=value;
}

/****************************************************
'@DESCRIPTION:	remove property
'@PARAM:	key [String] : property name
'****************************************************/
Object.prototype.removeer__=function(key){
	delete this[key];
}

/****************************************************
'@DESCRIPTION:	remove property
'@PARAM:	key [String] : property name
'****************************************************/
Object.prototype.globalize__=function(globalizedname){
	F.globalize(this,globalizedname);
}
</script>