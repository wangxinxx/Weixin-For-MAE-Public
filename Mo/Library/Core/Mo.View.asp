<%
Dim MoAspEnginer_View_Loaded:MoAspEnginer_View_Loaded=true
class MoAspEnginer_View
	private mvarDicts, mvarContent,loops,NewHash

	'****************************************************
	'@DESCRIPTION:	set template content
	'@PARAM:	value [String] : template content
	'****************************************************
	public Property Let Content(byval value)
		mvarContent = value
	end Property

	'****************************************************
	'@DESCRIPTION:	Class_Initialize
	'****************************************************
	private sub Class_Initialize()
		loops=";"
	end sub
	
	'****************************************************
	'@DESCRIPTION:	Class_Terminate
	'****************************************************
	private sub Class_Terminate()
		
	end sub

	'****************************************************
	'@DESCRIPTION:	template enginer entry
	'@PARAM:	content [String] : template content
	'@RETURN:	[Variant] combined asp code 
	'****************************************************
	public default function V__(byval content)
		content = replaceex(content,"<switch(.+?)>(\s*)<case","<switch$1><case")
		content= replace(content,vbcrlf,"--movbcrlf--")
		content= replace(content,chr(10),"--movbcrlf--")
		content= replace(content,chr(13),"--movbcrlf--")
		content= replace(content,"--movbcrlf--","--movbcrlf--" & vbcrlf)
		set mvarDicts = server.CreateObject("scripting.dictionary")
		mvarContent = content
		parsePreCombine()
		if MO_TAG_LIB<>"" then
			dim libs,lib,taglib,closetag,tagcontent
			libs = split(MO_TAG_LIB,",")
			for each lib in libs
				closetag = true
				tagcontent = ""
				set taglib = Mo("TagLib:Tag." & lib)
				dim match,matches,returnValue
				set matches = GetMatch(mvarContent,"\<" & lib & "\b([\s\S]*?)\>([\s\S]*?)\<\/" & lib & "\>")
				if matches.count>0 then
					closetag = false
				else
					set matches = GetMatch(mvarContent,"\<" & lib & "\b([\s\S]*?)\/\>")
				end if
				for each match in matches
					if closetag = false then tagcontent = match.submatches(1)
					mvarContent = Replace(mvarContent,match.value,taglib(F.readAttrs(match.submatches(0)),tagcontent))
				next
				set matches = nothing
			next
		end if
		parseSource()
		getLoops()
		parsePage()
		parseVari "#"
		parseVari "@"
		parseLoop()
		parseForeach()
		parseEmpty()
		parseSwitch()
		parseCompare "lt","<",""
		parseCompare "gt",">",""
		parseCompare "nlt","<","not"
		parseCompare "ngt",">","not"
		parseCompare "eq","=",""
		parseCompare "neq","=","not"
		parseExpression()
		parseExpressionElse()
		mvarContent = replace(mvarContent,"</else>","<?MoAsp else MoAsp?>")
		mvarContent = replace(mvarContent,"<else />","<?MoAsp else MoAsp?>")
		mvarContent = replace(mvarContent,"</switch>","<?MoAsp end select MoAsp?>")
		mvarContent = replace(mvarContent,"<default />","<?MoAsp case else MoAsp?>")
		mvarContent = replaceex(mvarContent,"</(n)?(eq|empty|lt|gt|expression)>","<?MoAsp end if MoAsp?>")
		parseVari ""
		parseAssignName()
		parseMoAsAsp()
		doSomethingToAsp()
		V__ =  mvarContent
	end function

	'****************************************************
	'@DESCRIPTION:	PreCombine the template with global values,such as "{$$MO_CORE}"
	'****************************************************
	private function parsePreCombine()
		dim matches,match,value
		set matches = GetMatch(mvarContent,"\{\$\$(\w+)\}")
		for each match in matches
			execute "value=" & match.submatches(0)
			mvarContent = replace(mvarContent,match.value,value)
		next
		set matches = nothing
	end function

	'****************************************************
	'@DESCRIPTION:	do something to the code that was combined to make the code pretty,such as remove blank lines
	'****************************************************
	private function doSomethingToAsp()
		if MO_DIRECT_OUTPUT then
			mvarContent = replace(mvarContent,"__Mo__.Echo ","T = T `&&` ")
		else
			mvarContent = "function Temp___()" & vbcrlf & "Dim TplStream" & vbcrlf & "Set TplStream=Server.CreateObject(""ADODB.STREAM"")" & vbcrlf & "TplStream.Mode=3" & vbcrlf & "TplStream.Type=2" & vbcrlf & "TplStream.Charset=MO_CHARSET" & vbcrlf & "TplStream.Open()" & vbcrlf & replace(mvarContent,"__Mo__.Echo ","T = T `&&` ") & vbcrlf & "TplStream.Position=0" & vbcrlf & "Temp___ = TplStream.ReadText()" & vbcrlf & "TplStream.Close()" & vbcrlf & "Set TplStream = Nothing" & vbcrlf & "end function"
		end if
		mvarContent = replaceex(mvarContent,"\r\nT \= T \`&&` ([^\""](.+?)[^\""])\r\nT \= T \`&&` ([^\""](.+?)[^\""])\r\n",vbcrlf & "T = T `&&` $1 `&&` $3 " & vbcrlf)
		mvarContent = replaceex(mvarContent,"T \= T \`&&` \""(.+)\""\r\nT \= T \`&&` ([^\""](.+?)[^\""])\r\nT \= T `&&` \""","T = T `&&` ""$1"" `&&` $2 `&&` """)
		mvarContent = replaceex(mvarContent,"T \= T \`&&` \""(.+)\""\r\nT \= T \`&&` ([^\""](.+?)[^\""])\r\nT \= T `&&` \""","T = T `&&` ""$1"" `&&` $2 `&&` """)
		mvarContent = replaceex(mvarContent,"T \= T \`&&` \""(.+)\""\r\nT \= T \`&&` ([^\""](.+?)[^\""])\r\nT \= T `&&` \""","T = T `&&` ""$1"" `&&` $2 `&&` """)
		'mvarContent = replaceex(mvarContent,"\r\nif (.+?) then\r\nT \= \T `&&` (.+?)\r\nend if\r\n",vbcrlf & "if $1 then T = T `&&` $2" & vbcrlf)
		mvarContent = replaceex(mvarContent,""" `&&` ""","")
		mvarContent = replaceex(mvarContent,"T = T `&&` ""--movbcrlf--""" & vbcrlf,"")
		
		mvarContent = replace(mvarContent,"--movbcrlf--""" & vbcrlf,""" `&&` vbcrlf" & vbcrlf)
		mvarContent = replace(mvarContent,"--movbcrlf--",""" `&&` vbcrlf `&&` """)
		mvarContent = replaceex(mvarContent," `&&` vbcrlf\r\nT \= T `&&` """," `&&` vbcrlf `&&` """)
		if MO_PREETY_HTML then
			mvarContent = replaceex(mvarContent,"(\s*)\"" `&&` vbcrlf `&&` \""(\s*)","")
			mvarContent = replaceex(mvarContent,"\bT \= T `&&` \""(\s+)","T = T `&&` """)
		end if
		if MO_DIRECT_OUTPUT then
			mvarContent = replace(mvarContent,"T = T `&&` ","Response.Write ")
			mvarContent = replace(mvarContent,"`&&`",vbcrlf & "Response.Write ")
		else
			mvarContent = replace(mvarContent,"T = T `&&` ","TplStream.WriteText ")
			mvarContent = replace(mvarContent,"`&&`",vbcrlf & "TplStream.WriteText ")
			mvarContent = replaceex(mvarContent,"\nTplStream\.WriteText (.+?)([ ]*)\r",vblf & "WriteStreamText TplStream,$1" & vbcr)
		end if
		if MO_PREETY_HTML then mvarContent = replace(mvarContent,vbcrlf & "WriteStreamText TplStream, vbcrlf" & vbcrlf,vbcrlf)
	end function

	'****************************************************
	'@DESCRIPTION:	parse page tag. i will call function 'CreatePageList' to create page string,if you do not define function property
	'****************************************************
	private function parsePage()
		dim matches,match,loopname,vbscript,pageurl,func,attrs,nloopname

		set matches = GetMatch(mvarContent,"\<page ([\s\S]+?)>([\s\S]*?)\</page>")
		if matches.count>0 then
			for each match in matches
				set attrs = F.readAttrs(match.submatches(0))
				if attrs.getter__("for")<>"" then
					mvarDicts("EOF_OF_" & attrs.getter__("for")) = match.submatches(1)
					mvarContent = replace(mvarContent,match.value,"<page " & match.submatches(0) & "/>")
				end if
			next
		end if
		
		set matches = GetMatch(mvarContent,"\<page ([\s\S]+?)/>")
		for each match in matches
			set attrs = F.readAttrs(match.submatches(0))
			if attrs.getter__("for")<>"" then
				loopname = attrs.getter__("for")
				nloopname = loopname
				pageurl = attrs.getter__("url")
				func = attrs.getter__("function")
				if func="" then func = "CreatePageList"
				if instr(loops,";" & loopname & ";")>0 then
					if not MO_COMPILE_STRICT then nloopname = "D__" & loopname
					mvarContent = replace(mvarContent,match.value,"<?MoAsp if Mo.Exists(""" & loopname & """) then MoAsp?><?MoAsp __Mo__.Echo " & func & "(""" & pageurl & """," & nloopname &".recordcount," & nloopname &".pagesize," & nloopname &".currentpage) MoAsp?><?MoAsp end if MoAsp?>")
				else
					mvarContent = replace(mvarContent,match.value,"")
				end if
			end if
		next
	end function

	'****************************************************
	'@DESCRIPTION:	get all loop tags.
	'****************************************************
	private function getLoops()
		dim matches,match,loopname,attrs
		set matches = GetMatch(mvarContent,"\<loop ([\s\S]+?)\>")
		for each match in matches
			set attrs = F.readAttrs(match.submatches(0))
			if attrs.getter__("name")<>"" then loops = loops & attrs.getter__("name") & ";"
		next
		set matches = nothing
	end function

	'****************************************************
	'@DESCRIPTION:	parse loop tag. All looped data is an instance of list(__Obj__) which is defined in 'Mo.List.asp'
	'****************************************************
	private function parseLoop()
		dim matches,match,loopname,vbscript,attrs,varname
		set matches = GetMatch(mvarContent,"\<loop ([\s\S]+?)\>")
		for each match in matches
			set attrs = F.readAttrs(match.submatches(0))
			if attrs.getter__("name")<>"" then
				loopname = attrs.getter__("name")
				varname = loopname
				if not MO_COMPILE_STRICT then varname = "D__" & loopname
				vbscript = "<?MoAsp "
				if not MO_COMPILE_STRICT then vbscript = vbscript & "if Mo.Exists(""" & loopname & """) then" & vbcrlf
				if not MO_COMPILE_STRICT then vbscript = vbscript & "set " & varname & " = Mo.value(""" & loopname & """)" & vbcrlf
				vbscript = vbscript & varname & ".Reset()" & vbcrlf
				dim contenteof : contenteof = attrs.getter__("eof")
				if contenteof<>"" then contenteof = replace(replace(contenteof,"&gt;",">"),"&lt;","<")
				if mvarDicts.exists("EOF_OF_" & loopname) then contenteof = mvarDicts("EOF_OF_" & loopname)
				contenteof = replace(replace(contenteof,"""",""""""),vbcrlf,"")
				if contenteof<>"" then vbscript = vbscript & "if " & varname & ".eof() then" & vbcrlf & "__Mo__.Echo """ & contenteof & """" & vbcrlf & "end if" & vbcrlf
				vbscript = vbscript & "K__" & loopname & "=" & varname & ".pagesize *(" & varname & ".currentpage-1)" & vbcrlf
				vbscript = vbscript & "do while not " & varname & ".eof()" & vbcrlf & "K__" & loopname & "=K__" & loopname & "+1" & vbcrlf & "set C__" & loopname & " =  " & varname & ".read() MoAsp?>"
				mvarContent = replace(mvarContent,match.value,vbscript)
				mvarContent = replace(mvarContent,"{$" & loopname &".Key__}","<?MoAsp __Mo__.Echo K__" & loopname & " MoAsp?>")
				dim m_,ms_
				set ms_ = GetMatch(mvarContent,"\{\$" & loopname & "\.(.+?)\}")
				for each m_ in ms_
					dim k,v:k = m_.submatches(0)
					if instr(k,":")<=0 then
						if MO_COMPILE_STRICT then
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo C__" & loopname & "." & k & " MoAsp?>")
						else
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo C__" & loopname & ".getter__(""" & k & """) MoAsp?>")
						end if
					else
						dim c
						c = mid(k,instr(k,":")+1)
						k = mid(k,1,instr(k,":")-1)
						if MO_COMPILE_STRICT then
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & replace(parseFormatVari(c),"{{k}}","C__" & loopname & "." & k) & " MoAsp?>")
						else
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & replace(parseFormatVari(c),"{{k}}","C__" & loopname & ".getter__(""" & k & """)") & " MoAsp?>")
						end if
					end if		
				next
			end if
		next
		if not MO_COMPILE_STRICT then 
			mvarContent = replace(mvarContent,"</loop>","<?MoAsp Loop " & vbcrlf & "End If MoAsp?>")	
		else
			mvarContent = replace(mvarContent,"</loop>","<?MoAsp Loop MoAsp?>")	
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	parse foreach tag.
	'****************************************************
	private function parseForeach()
		dim matches,match,loopname,vbscript,typ,basezero,attrs
		'set matches = GetMatch(mvarContent,"\<foreach name\=\""([\w\.]+?)\""( type\=\""(.+?)\"")?( basezero\=\""(true)\"")?\>")
		set matches = GetMatch(mvarContent,"\<foreach ([\s\S]+?)\>")
		for each match in matches
			dim m_,ms_, k,v,c
			set attrs = F.readAttrs(match.submatches(0))
			if attrs.getter__("name")<>"" then
				loopname = attrs.getter__("name")
				typ = attrs.getter__("type")
				vbscript = "<?MoAsp "
				if not MO_COMPILE_STRICT then vbscript = vbscript & "if Mo.Exists(""" & loopname & """) then" & vbcrlf
				basezero = 0
				if attrs.getter__("basezero")="true" then basezero=-1
				if typ="object" then
					if not MO_COMPILE_STRICT then 
						vbscript = vbscript & "K__" & loopname & "=" & basezero & vbcrlf & "for each C__" & loopname & " in Mo.value(""" & loopname & """)" & vbcrlf & "K__" & loopname & "=K__" & loopname & "+1" & vbcrlf & " MoAsp?>" & vbcrlf
					else
						vbscript = vbscript & "K__" & loopname & "=" & basezero & vbcrlf & "for each C__" & loopname & " in " & loopname & vbcrlf & "K__" & loopname & "=K__" & loopname & "+1" & vbcrlf & " MoAsp?>" & vbcrlf
					end if
					mvarContent = replace(mvarContent,match.value,vbscript)
					mvarContent = replace(mvarContent,"{$" & loopname &".Key__}","<?MoAsp __Mo__.Echo K__" & loopname & " MoAsp?>")
					set ms_ = GetMatch(mvarContent,"\{\$" & loopname & "\.(.+?)\}")
					for each m_ in ms_
						k = m_.submatches(0)
						if instr(k,":")<=0 then
							if MO_COMPILE_STRICT then
								mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo C__" & loopname & "." & k & " MoAsp?>")
							else
								mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo C__" & loopname & ".getter__(""" & k & """) MoAsp?>")
							end if
						else
							c = mid(k,instr(k,":")+1)
							k = mid(k,1,instr(k,":")-1)
							if MO_COMPILE_STRICT then
								mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & replace(parseFormatVari(c),"{{k}}","C__" & loopname & "." & k) & " MoAsp?>")
							else
								mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & replace(parseFormatVari(c),"{{k}}","C__" & loopname & ".getter__(""" & k & """)") & " MoAsp?>")
							end if
						end if		
					next
				else
					if not MO_COMPILE_STRICT then 
						vbscript = vbscript & "K__" & loopname & "=" & basezero & vbcrlf & "for each C__" & loopname & " in Mo.value(""" & loopname & """)" & vbcrlf & "K__" & loopname & "=K__" & loopname & "+1 MoAsp?>"
					else
						vbscript = vbscript & "K__" & loopname & "=" & basezero & vbcrlf & "for each C__" & loopname & " in " & loopname & vbcrlf & "K__" & loopname & "=K__" & loopname & "+1 MoAsp?>"
					end if
					mvarContent = replace(mvarContent,match.value,vbscript)
					mvarContent = replace(mvarContent,"{$" & loopname &".Key__}","<?MoAsp __Mo__.Echo K__" & loopname & " MoAsp?>")
					set ms_ = GetMatch(mvarContent,"\{\$" & loopname & "(\:(.+?))?\}")
					for each m_ in ms_
						k = m_.submatches(1)
						if k="" then
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo C__" & loopname & " MoAsp?>")
						else
							mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & replace(parseFormatVari(k),"{{k}}","C__" & loopname & "") & " MoAsp?>")
						end if		
					next			
				end if
			end if
		next
		if not MO_COMPILE_STRICT then 
			mvarContent = replace(mvarContent,"</foreach>","<?MoAsp Next " & vbcrlf & "End If MoAsp?>")	
		else
			mvarContent = replace(mvarContent,"</foreach>","<?MoAsp Next MoAsp?>")
		end if
	end function	

	'****************************************************
	'@DESCRIPTION:	parse switch tag
	'****************************************************
	private function parseSwitch()
		dim matches,m_,attrs
		set matches = GetMatch(mvarContent,"<switch ([\s\S]+?)>")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(0))
			if attrs.getter__("name")<>"" then mvarContent = replace(mvarContent,m_.value,"<?MoAsp select case " & parseAssign(attrs.getter__("name")) & " MoAsp?>")
		next
		parseCase()
	end function

	'****************************************************
	'@DESCRIPTION:	parse case tag
	'****************************************************
	private function parseCase()
		dim matches,m_,t,quto,attrs
		set matches = GetMatch(mvarContent,"<case ([\s\S]+?)/>")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(0))
			quto=""""
			if instr("bool|number|money|date|assign","|" & attrs.getter__("type") & "|")>0 then quto=""
			mvarContent = replace(mvarContent,m_.value,"<?MoAsp case " & quto & attrs.getter__("value") & quto & " MoAsp?>")
		next
	end function
	
	'****************************************************
	'@DESCRIPTION:	parse expression
	'****************************************************
	private function parseExpression()
		dim matches,m_,expression
		set matches = GetMatch(mvarContent,"<expression ([\s\S]+?)>")
		for each m_ in matches
			expression = parseExpressionComponent(m_.submatches(0))
			if expression="" then F.exit "template parse error,please check 'expression' tag��"
			mvarContent = replace(mvarContent,m_.value,"<?MoAsp if " & expression & " then MoAsp?>")
		next
		mvarContent = replaceex(mvarContent,"<(and|or)(.+?)/>","")
	end function
	private function parseExpressionElse()
		dim matches,m_,expression
		set matches = GetMatch(mvarContent,"<else ([\s\S]+?) />")
		for each m_ in matches
			expression = parseExpressionComponent(m_.submatches(0))
			if expression="" then F.exit "template parse error,please check 'expression' tag��"
			mvarContent = replace(mvarContent,m_.value,"<?MoAsp elseif " & expression & " then MoAsp?>")
		next
	end function
	private function parseExpressionComponent(compare)
		dim expression,submatches,n_,v_,varmatches,j_,quto,vv_
		expression=""
		set submatches = GetMatch(compare,"\b(and|or|group)\=\""(.+?)\""")
		for each n_ in submatches
			if n_.submatches(0)="and" or n_.submatches(0)="or" then
				v_ = n_.submatches(1)
				set varmatches = GetMatch(v_,"^(.+?)((\s)(\+|\-|\*|\/|mod)(\s)([\d\.e\+]+))?(\s)(gt|lt|ngt|nlt|eq|neq)(\s)(.+?)((\s)as(\s)(bool|number|money|date|assign))?$")
				if varmatches.count>0 then
					quto=""""
					vv_ = varmatches(0).submatches(9)
					if expression<>"" then expression = expression & " _" & vbcrlf & n_.submatches(0) & " "
					if left(varmatches(0).submatches(7),1)="n" then expression = expression & " not "
					if right(varmatches(0).submatches(7),1)="t" then quto=""
					if vv_="Empty" then
						expression = expression & " is_empty("
						expression = expression & parseAssign(varmatches(0).submatches(0))
						if varmatches(0).submatches(1)<>"" then expression = expression & varmatches(0).submatches(1)
						expression = expression & ") "
					else
						expression = expression & parseAssign(varmatches(0).submatches(0))
						if varmatches(0).submatches(1)<>"" then expression = expression & varmatches(0).submatches(1)
						if varmatches(0).submatches(7)="gt" then expression = expression & " > "
						if varmatches(0).submatches(7)="lt" then expression = expression & " < "
						if varmatches(0).submatches(7)="ngt" then expression = expression & " > "
						if varmatches(0).submatches(7)="nlt" then expression = expression & " < "
						if varmatches(0).submatches(7)="eq" then expression = expression & " = "
						if varmatches(0).submatches(7)="neq" then expression = expression & " = "
						if varmatches(0).submatches(10)<>"" then
							quto=""
							if varmatches(0).submatches(13)="assign" then vv_ = parseAssign(vv_)
						end if
						if RegTest(vv_,"^\{\$(.+?)\}$") then
							vv_ = parseAssign(replaceex(vv_,"^\{\$(.+?)\}$","$1"))
							quto=""
						end if
						expression = expression & quto & vv_ & quto
					end if
				end if
			else
				dim group:group = n_.submatches(1)
				dim groupmatches:set groupmatches = GetMatch(mvarContent,"<(and|or) name\=\""" & group & "\""(.+?) \/>")
				if groupmatches.count>0 then
					expression = expression & " _" & vbcrlf & groupmatches(0).submatches(0) & " (" & parseExpressionComponent(groupmatches(0).submatches(1)) & ") "
				end if
			end if
		next
		parseExpressionComponent = expression
	end function
	
	'****************************************************
	'@DESCRIPTION:	parse compare tag(gt,lt,ngt,nlt,eq,neq)
	'@PARAM:	tag [String] : gt/lt/ngt/nlt/eq/neq
	'@PARAM:	comp [String] : >/</= . The symbol which i will compare with
	'@PARAM:	no [String] : not/[blank]. The value 'not' follows the type of the tag.
	'****************************************************
	private function parseCompare(tag,comp,no)
		dim matches,m_,attrs
		set matches = GetMatch(mvarContent,"<" & tag & " ([\s\S]+?)>")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(0))
			if attrs.getter__("name")<>"" then
				newexpression = attrs.getter__("name") & " " & tag & " " & attrs.getter__("value")
				if attrs.getter__("type")<>"" then newexpression =  newexpression & " as " & attrs.getter__("type")
				mvarContent = replace(mvarContent,m_.value,"<expression and=""" & newexpression & """>")
			end if
		next
	end function

	'****************************************************
	'@DESCRIPTION:	parse empty tag
	'****************************************************
	private function parseEmpty()
		dim matches,m_,l,k,v,s,attrs
		'set matches = GetMatch(mvarContent,"<(n)?empty name\=\""((([\w]+?)\.)?(.+?))\"">")
		set matches = GetMatch(mvarContent,"<(n)?empty ([\s\S]+?)>")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(1))
			if attrs.getter__("name")<>"" then
				s=""
				if m_.submatches(0)="n" then s=" not"
				mvarContent = replace(mvarContent,m_.value,"<?MoAsp if" & s & " is_empty(" & parseAssign(attrs.getter__("name")) & ") then MoAsp?>")
			end if
		next
	end function

	'****************************************************
	'@DESCRIPTION:	parse assign tag
	'****************************************************
	private function parseAssignName()
		dim matches,m_,attrs
		'set matches = GetMatch(mvarContent,"<assign name\=\""(\w+?)\"" value=\""(.+?)\"" />")
		set matches = GetMatch(mvarContent,"<assign ([\s\S]+?)/>")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(0))
			if attrs.getter__("name")<>"" then mvarContent = replace(mvarContent,m_.value,"<?MoAsp Mo.Assign """ & attrs.getter__("name") & """,""" & replace(attrs.getter__("value"),"""","""""") & """ MoAsp?>")
		next
	end function

	'****************************************************
	'@DESCRIPTION:	parse source include tag(css/js/load)
	'****************************************************
	private function parseSource()
		dim matches,m_,id,cs,ext,attrs,filepath
		'set matches = GetMatch(mvarContent,"<(js|css|load) (file|href|src)\=\""(.+?)\""( id\=\""(.+?)\"")?( charset\=\""(.+?)\"")? />")
		set matches = GetMatch(mvarContent,"<(js|css|load) ([\s\S]+?) />")
		for each m_ in matches
			set attrs = F.readAttrs(m_.submatches(1))
			filepath = ""
			if attrs.getter__("file")<>"" then filepath = attrs.getter__("file")
			if attrs.getter__("href")<>"" then filepath = attrs.getter__("href")
			if attrs.getter__("src")<>"" then filepath = attrs.getter__("src")
			if filepath<>"" then
				id=""
				cs=""
				if instrrev(filepath,".")>0 then ext = lcase(mid(filepath,instrrev(filepath,".")))
				if attrs.getter__("id")<>"" then id=" id=""" & attrs.getter__("id") & """"
				if attrs.getter__("charset")<>"" then cs=" charset=""" & attrs.getter__("charset") & """"
				if m_.submatches(0)="js" or ext=".js" then
					mvarContent = replace(mvarContent,m_.value,"<script type=""text/javascript"" src=""" & filepath & """" & id & cs & "></script>")
				else
					mvarContent = replace(mvarContent,m_.value,"<link rel=""stylesheet"" type=""text/css"" href=""" & filepath & """" & id & cs & " />")
				end if
			end if
		next
	end function

	'****************************************************
	'@DESCRIPTION:	parse single variable
	'@PARAM:	chars [Variant] : #/@/[blank]
	'****************************************************
	private function parseVari(chars)
		dim matches,m_
		set matches = GetMatch(mvarContent,"\{\$" & chars & "(.+?)\}")
		for each m_ in matches
			if chars="#" then
				mvarContent = replace(mvarContent,m_.value,""" & " & parseAssign(m_.submatches(0)) & " & """)
			elseif chars="@" then
				mvarContent = replace(mvarContent,m_.value,parseAssign(m_.submatches(0)))
			else
				mvarContent = replace(mvarContent,m_.value,"<?MoAsp __Mo__.Echo " & parseAssign(m_.submatches(0)) & " MoAsp?>")	
			end if
		next
	end function

	'****************************************************
	'@DESCRIPTION:	parse assign variable as asp code
	'@PARAM:	key [Variant] : variable need to be parsed
	'****************************************************
	private function parseAssign(byval key)
		dim k,v,m_,ms_,l,c,cf,kn:k=key
		set ms_ = Getmatch(key,"^([\w\.]+?)(\:(.+?))?$")
		if ms_.count>0 then
			l = ms_(0).submatches(0)
			c = ms_(0).submatches(2)
			if c="" then
				if instr(l,".")<=0 then 
					if MO_COMPILE_STRICT then parseAssign = l else parseAssign = "Mo.value(""" & l & """)"
				elseif lcase(left(l,2))="c." or lcase(left(l,2))="g." then
					parseAssign = mid(l,3)
				elseif lcase(left(l,11))="mo.session." then
					parseAssign = "F.session(""" & mid(l,12) & """)"
				elseif lcase(left(l,11))="mo.get.int." then
					parseAssign = "F.get.int(""" & mid(l,12) & """,0)"
				elseif lcase(left(l,12))="mo.post.int." then
					parseAssign = "F.post.int(""" & mid(l,13) & """,0)"
				elseif lcase(left(l,7))="mo.get." then
					parseAssign = "F.get(""" & mid(l,8) & """)"
				elseif lcase(left(l,8))="mo.post." then
					parseAssign = "F.post(""" & mid(l,9) & """)"
				elseif lcase(left(l,10))="mo.cookie." then
					parseAssign = "F.cookie(""" & mid(l,11) & """)"
				elseif lcase(left(l,10))="mo.server." then
					parseAssign = "F.server(""" & mid(l,11) & """)"
				elseif lcase(left(l,5))="mo.l." then
					parseAssign = "Mo.L(""" & mid(l,6) & """)"
				elseif lcase(left(l,5))="mo.c." then
					cf = mid(l,6)
					if instr(cf,".")>0 then 
						parseAssign = "Mo.C(""" & mid(cf,1,instr(cf,".")-1) & """)." & mid(cf,instr(cf,".")+1)
					end if
				elseif lcase(left(l,5))="mo.a." then
					cf = mid(l,6)
					if instr(cf,".")>0 then 
						parseAssign = "Mo.A(""" & mid(cf,1,instr(cf,".")-1) & """)." & mid(cf,instr(cf,".")+1)
					end if
				else
					k = mid(l,instr(l,".")+1)
					l = left(l,instr(l,".")-1)
					if instr(loops,";" & l & ";")>0 then
						if MO_COMPILE_STRICT then parseAssign = "C__" & l & "." & k else parseAssign = "C__" & l & ".getter__(""" & k & """)"
					else
						if MO_COMPILE_STRICT then parseAssign = l & "." & k else parseAssign = "Mo.Values(""" & l & """,""" & k & """)"
					end if
				end if
			else
				if instr(l,".")<=0 then 
					if MO_COMPILE_STRICT then parseAssign = replace(parseFormatVari(c),"{{k}}",l) else parseAssign = replace(parseFormatVari(c),"{{k}}","Mo.value(""" & l & """)")
				elseif lcase(left(l,2))="c." or lcase(left(l,2))="g." then
					parseAssign = replace(parseFormatVari(c),"{{k}}",mid(l,3))
				elseif lcase(left(l,11))="mo.session." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.session(""" & mid(l,12) & """)")
				elseif lcase(left(l,11))="mo.get.int." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.get.int(""" & mid(l,12) & """,0)")
				elseif lcase(left(l,12))="mo.post.int." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.post.int(""" & mid(l,13) & """,0)")
				elseif lcase(left(l,7))="mo.get." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.get(""" & mid(l,8) & """)")
				elseif lcase(left(l,8))="mo.post." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.post(""" & mid(l,9) & """)")
				elseif lcase(left(l,10))="mo.cookie." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.cookie(""" & mid(l,11) & """)")
				elseif lcase(left(l,10))="mo.server." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","F.server(""" & mid(l,11) & """)")
				elseif lcase(left(l,5))="mo.l." then
					parseAssign = replace(parseFormatVari(c),"{{k}}","Mo.L(""" & mid(l,6) & """)")
				elseif lcase(left(l,5))="mo.c." then
					cf = mid(l,6)
					if instr(cf,".")>0 then 
						parseAssign = replace(parseFormatVari(c),"{{k}}","Mo.C(""" & mid(cf,1,instr(cf,".")-1) & """)." & mid(cf,instr(cf,".")+1))
					end if
				elseif lcase(left(l,5))="mo.a." then
					cf = mid(l,6)
					if instr(cf,".")>0 then 
						parseAssign = replace(parseFormatVari(c),"{{k}}","Mo.A(""" & mid(cf,1,instr(cf,".")-1) & """)." & mid(cf,instr(cf,".")+1))
					end if
				else
					k = mid(l,instr(l,".")+1)
					l = left(l,instr(l,".")-1)
					if instr(loops,";" & l & ";")>0 then
						if MO_COMPILE_STRICT then parseAssign = replace(parseFormatVari(c),"{{k}}","C__" & l & "." & k) else parseAssign = replace(parseFormatVari(c),"{{k}}","C__" & l & ".getter__(""" & k & """)")
					else
						if MO_COMPILE_STRICT then parseAssign = replace(parseFormatVari(c),"{{k}}",l & "." & k) else parseAssign = replace(parseFormatVari(c),"{{k}}","Mo.Values(""" & l & """,""" & k & """)")
					end if
				end if
			end if
		end if
	end function

	'****************************************************
	'@DESCRIPTION:	parse special variable(function or inner object ref)
	'@PARAM:	format [String] : format string
	'****************************************************
	private function parseFormatVari(format)
		parseFormatVari = ""
		if format="" then exit function
		dim func,vars,ret
		func = format
		if instr(func,"=")>0 then 
			vars ="," & mid(func,instr(func,"=")+1)
			func = mid(func,1,instr(func,"=")-1)
		end if
		
		if left(lcase(func),3)="mo." and ubound(split(func,"."))=2 then
			func = "Mo.Static(""" & split(func,".")(1) & """)." & split(func,".")(2)
		elseif left(lcase(func),5)="mo.a." and ubound(split(func,"."))=3 then
			func = "Mo.A(""" & split(func,".")(2) & """)." & split(func,".")(3)
		elseif left(lcase(func),2)="f." and len(func)>2 then
			func = "F." & mid(func,3)
		end if
		if instr("," & lcase(DISABLED_FUNCTIONS) & ",","," & lcase(func) & ",") then
			parseFormatVari = """disabled function:" & func & """"
			exit function
		end if
		ret=func & "({{k}}" & vars & ")"
		if right(vars,3)="..." Then
			vars = left(vars,len(vars)-3)
			if left(vars,1)="," then vars = right(vars,len(vars)-1)
			ret = func & "(" & vars & "{{k}})"
		elseif instr(vars,",...,")>0 Then
			vars = replace(vars,",...,",",{{k}},")
			if left(vars,1)="," then vars = right(vars,len(vars)-1)
			ret = func & "(" & vars & ")"
		end if
		parseFormatVari = ret
	end function

	private sub parseMoAsAsp()
		dim m_,ms_,id
		set ms_ = getmatch(mvarContent,"<\?MoAsp([\s\S]+?)MoAsp\?>")
		for each m_ in ms_
			id = getRndid()
			mvarDicts(id) = m_.value
			mvarContent = replace(mvarContent,m_.value,vbcrlf & "<?MoAsp" & id & "MoAsp?>" & vbcrlf)
		next
		mvarContent = replaceex(mvarContent,"(^(\s+)|(\s+)$)","")
		mvarContent = replaceex(mvarContent,"(\r\n){2,}",vbcrlf)
		mvarContent = replace(mvarContent,"""","""""")
		mvarContent = replaceex(mvarContent,"(^|\r\n)","$1__Mo__.Echo """)
		mvarContent = replaceex(mvarContent,"($|\r\n)","""$1" )
		set ms_ = getmatch(mvarContent,"__Mo__\.Echo ""<\?MoAsp([\w]+?)MoAsp\?>""")
		for each m_ in ms_
			id = m_.submatches(0)
			mvarContent = replace(mvarContent,m_.value,mvarDicts(id))
		next
		mvarContent = replace(mvarContent,"<?MoAsp ","")
		mvarContent = replace(mvarContent," MoAsp?>","")
		'mvarContent = replace(mvarContent,"""" & vbcrlf & "__Mo__.Echo ""","")
		'mvarContent = replace(mvarContent,"""" & vbcrlf & "__Mo__.Echo ",""" & ")
	end sub
	
	private function getRndid()
		dim rid
		rid = RndStr1(10)
		do while mvarDicts.exists(rid)
			rid = RndStr1(10)
		loop
		getRndid = rid
	end function
end class
%>