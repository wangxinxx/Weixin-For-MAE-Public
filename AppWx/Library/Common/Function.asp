﻿<%
function TestArg(arg,src,arg2)
	TestArg = arg & " => " & src & " => " & arg2
end function

'****************************************************
'@DESCRIPTION:	Get ChrB for Jscript
'@PARAM:	c [Int] : char code
'@RETURN:	[Byte]
'****************************************************
function GetChrB(c)
	GetChrB = chrb(c)
end function

function ParseMessage(detail,t)
	if not is_empty(detail) then
		dim json
		if instr(detail,"{")>0 then set json = F.json(detail)
		if t="text" then
			ParseMessage = "文本</td><td rel=""" & t & """>" & F.encodeHTML(left(json.getter__("Content"),50))
		elseif t="image" then
			if json.getter__("PicUrl")<>"" then
				ParseMessage = "图片</td><td rel=""" & t & """>" & "<a href=""" & json.getter__("PicUrl") & """ target=""_blank"">查看图片</a>"
			else
				ParseMessage = "图片</td><td rel=""" & t & """>" & "<a href=""?m=Media&MediaId=" & json.getter__("MediaId") & """ target=""_blank"">图片资源</a>"
			end if
		elseif t="voice" then
			ParseMessage = "语音</td><td rel=""" & t & """>" & "<a href=""?m=Media&MediaId=" & json.getter__("MediaId") & """>声音资源</a>"
		elseif t="video" then
			ParseMessage = "视频</td><td rel=""" & t & """>" & "<a href=""?m=Media&MediaId=" & json.getter__("Mediaid") & """>视频资源</a>"
		elseif t="mutinews" then
			if not isempty(json) then
				ParseMessage = "图文</td><td rel=""" & t & """>" & F.encodeHTML(json.List.getter__(0).getter__("Title"))
			else
				ParseMessage = "图文</td><td rel=""" & t & """>多条图文"
			end if
		elseif t="news" then
			if json.getter__("Title")<>"" then
				ParseMessage = "图文</td><td rel=""" & t & """>" & F.encodeHTML(json.getter__("Title"))
			else
				ParseMessage = "图文</td><td rel=""" & t & """>" & F.encodeHTML(json.List.getter__(0).getter__("Title"))
			end if
		elseif t="music" then
			ParseMessage = "音乐</td><td rel=""" & t & """>" & F.encodeHTML(json.getter__("Title")) & "，" & json.getter__("MusicUrl")
		elseif t="location" then
			ParseMessage = "位置</td><td rel=""" & t & """>" & json.getter__("Label")
		elseif t="link" then
			ParseMessage = "链接</td><td rel=""" & t & """>" & "<a href=""" & json.getter__("Url") & """ target=""_blank"">" & F.encodeHTML(json.getter__("Title")) & "</a>"
		elseif t="event" then
			if json.getter__("Event")="LOCATION" then
				ParseMessage = "事件</td><td rel=""" & t & """>LOCATION(" & json.getter__("Latitude") & "," & json.getter__("Longitude") & ")"
			elseif json.getter__("Event")="subscribe" then
				ParseMessage = "事件</td><td rel=""" & t & """>关注"
			elseif json.getter__("Event")="unsubscribe" then
				ParseMessage = "事件</td><td rel=""" & t & """>取消关注"
			else
				ParseMessage = "事件</td><td rel=""" & t & """>" & json.getter__("Event")
				if json.getter__("EventKey")<>"" then ParseMessage = "事件</td><td rel=""" & t & """>" & json.getter__("Event") & "(" & json.getter__("EventKey") & ")"
			end if
		end if
	else
		ParseMessage = "无"
	end if
end function
function CheckIn(id,list)
	CheckIn = ""
	if instr("," & list & ",","," & id & ",")>0 then CheckIn = " selected=""selected"""
end function
function WriteOperateLog(Byval Description,Byval Status,Byval UserName)
	if UserName="" then UserName = F.session("wx_auth_name")
	if is_empty(UserName) then exit function
	Model__("OperateLog","Id").insert _
	"UserName",UserName,_
	"WxTag",F.session.int("Id",0),_
	"ClientIpAddress",F.server("REMOTE_ADDR"),_
	"OperateDateTime",F.timespan(),_
	"Model",Mo.Method,_
	"Action",Mo.Action,_
	"Description",Description,_
	"Status",Status
end function
%>