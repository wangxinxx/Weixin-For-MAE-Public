<script language="jscript" runat="server">
Mo.Use("WXServices");
function MoLibWXServicesKf(appID,appsecret){
	MoLibWXServices.apply(this,[appID,appsecret]);
	this.News={
		length:0,
		push:function(value){
			this[this.length++]=value;
		},
		clear:function(){
			while(this.length>0){
				this[--this.length]=null;
			}
		},
		append:function(title, content, url, pic){
			this.push({"title":title, "content":content, "pic":pic||"", "url":url||""});
		}
	};
}
MoLibWXServicesKf.prototype = new MoLibWXServices();
MoLibWXServicesKf.New = function(appID,appsecret){return new MoLibWXServicesKf(appID,appsecret);};

MoLibWXServicesKf.prototype.sendmessage = function(json){
	return this.parseerror(MoLibHttpRequest.New("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + this.Auth.access_token,"POST",json).getjson("utf-8"));
};
MoLibWXServicesKf.prototype.sendtext = function(openid,content){
	Mo.Use("JsonGenerater");
	var Json = MoLibJsonGenerater.New();
	Json.put("touser",openid);
	Json.put("msgtype","text");
	Json.putnew("text").put("content",content);
	return this.sendmessage(MoLibJsonParser.unParse(Json.table));
};
MoLibWXServicesKf.prototype.sendimage = function(openid,mediaid){
	return this.sendmessage(F.format("{\"touser\":\"{0}\",\"msgtype\":\"image\",\"image\":{\"media_id\":\"{1}\"}}",openid,mediaid));
};
MoLibWXServicesKf.prototype.sendvoice = function(openid,mediaid){
	return this.sendmessage(F.format("{\"touser\":\"{0}\",\"msgtype\":\"voice\",\"voice\":{\"media_id\":\"{1}\"}}",openid,mediaid));
};
MoLibWXServicesKf.prototype.sendvideo = function(openid,mediaid,title,description){
	Mo.Use("JsonGenerater");
	var Json = MoLibJsonGenerater.New();
	Json.put("touser",openid);
	Json.put("msgtype","video");
	var video = Json.putnew("video")
	video.put("media_id",mediaid);
	video.put("title",title||"");
	video.put("description",description||"");
	return this.sendmessage(MoLibJsonParser.unParse(Json.table));
};
MoLibWXServicesKf.prototype.sendmusic = function(openid,mediaid,musicurl,title,description,hqmusicurl){
	if(hqmusicurl===undefined || hqmusicurl=="")hqmusicurl=musicurl;
	Mo.Use("JsonGenerater");
	var Json = MoLibJsonGenerater.New();
	Json.put("touser",openid);
	Json.put("msgtype","music");
	var video = Json.putnew("music")
	video.put("title",title||"");
	video.put("description",description||"");
	video.put("musicurl",musicurl);
	video.put("hqmusicurl",hqmusicurl);
	video.put("thumb_media_id",mediaid);
	return this.sendmessage(MoLibJsonParser.unParse(Json.table));
};
MoLibWXServicesKf.prototype.sendnews = function(openid){
	if(this.News.length<=0)return this.parseerror({"errcode":1,"errmsg":"no news to send"});
	Mo.Use("JsonGenerater");
	var Json = MoLibJsonGenerater.New();
	Json.put("touser",openid);
	Json.put("msgtype","news");
	var video = Json.putnew("news").putnewarray("articles")
	for(var i=0;i<this.News.length;i++){
		var news = video.putnew();
		news.put("title",this.News[i].title||"");
		news.put("description",this.News[i].content||"");
		news.put("url",this.News[i].url||"");
		news.put("picurl",this.News[i].pic||"");
	}
	return this.sendmessage(MoLibJsonParser.unParse(Json.table));
}
</script>