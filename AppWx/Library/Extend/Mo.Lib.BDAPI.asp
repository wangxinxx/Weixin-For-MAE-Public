<script language="jscript" runat="server">
function MoLibBDAPI(ak,sk){
	this.ak = ak ||"";
	this.sk = sk ||"";
	this.output="json";
	this.sn = "";
	this.url = "http://api.map.baidu.com/geocoder/v2/";
	this.uri = "/geocoder/v2/";
	Mo.Use("HttpRequest");
}
MoLibBDAPI.COORDTYPE={
	"BAIDU":"bd09ll",
	"GCJ":"gcj02ll",
	"WGS":"wgs84ll"
};
MoLibBDAPI.New = function(ak,sk){return new MoLibBDAPI(ak,sk);};
MoLibBDAPI.prototype.GPSConvert = function(coords,from,to){
	this.sn="";
	from = from || 1;
	to = to || 5;
	this.uri="/geoconv/v1/?ak=" + this.ak + "&output=" + this.output + "&coords=" + F.encode(coords) + "&from=" + from + "&to=" + to;
	if(this.sk!="")this.sn = F.md5(encodeURIComponent(this.uri+this.sk));
	var http = MoLibHttpRequest.New("http://api.map.baidu.com" + this.uri + "&sn=" + this.sn);
	if(http.getjson1("utf-8")){
		if(http.response.status==0)return {"X":http.response.result[0].x,"Y":http.response.result[0].y};
		return {"X":0,"Y":0}
	}
	return {"X":0,"Y":0};
};
MoLibBDAPI.prototype.getGPSFromAddress = function(address,city){
	city = city ||"";
	this.sn="";
	if(this.sk!="")this.sn = F.md5(this.uri+this.sk);
	var http = MoLibHttpRequest.New("http://api.map.baidu.com/geocoder/v2/?ak=" + this.ak + "&sn=" + this.sn + "&output=" + this.output + "&address=" + F.encode(address) + "&city=" + F.encode(city));
	return http.getjson("utf-8");
};
MoLibBDAPI.prototype.getAddressFromGPS = function(location,coordtype,pois){
	if(pois===undefined)pois=0;
	coordtype = coordtype || MoLibBDAPI.COORDTYPE.WGS;
	this.sn="";
	if(this.sk!="")this.sn = F.md5(this.uri+this.sk);
	var http = MoLibHttpRequest.New("http://api.map.baidu.com/geocoder/v2/?ak=" + this.ak + "&sn=" + this.sn + "&output=" + this.output + "&location=" + F.encode(location) + "&coordtype=" + F.encode(coordtype) + "&pois=" + F.encode(pois));
	return http.getjson("utf-8");
};
</script>