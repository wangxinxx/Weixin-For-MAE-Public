<script language="jscript" runat="server">
Mo.Use("WXServices");
function MoLibWXServicesMuti(appID,appsecret){
	MoLibWXServices.apply(this,[appID,appsecret]);
	this.sendto="group";
	this.filter="";
	this.News={
		length:0,
		push:function(value){
			this[this.length++]=value;
		},
		clear:function(){
			while(this.length>0){
				this[--this.length]=null;
			}
		},
		append:function(thumb_media_id,title,content,content_source_url,author,digest){
			this.push({
				"thumb_media_id":thumb_media_id, 
				"title":title, 
				"content":content, 
				"content_source_url":content_source_url||"", 
				"author":author||"", 
				"digest":digest||""
			});
		}
	};
}
MoLibWXServicesMuti.prototype = new MoLibWXServices();
MoLibWXServicesMuti.New = function(appID,appsecret){return new MoLibWXServicesMuti(appID,appsecret);};
MoLibWXServicesMuti.prototype.createfilter = function(){
	if(this.sendto="group"){
		return "\"filter\":{\"group_id\":\"" + this.filter + "\"}";
	}else{
		return "\"touser\":[\"" + this.filter.replace(/\,/igm,"\",\"") + "\"]";
	}
};
MoLibWXServicesMuti.prototype.uploadnews = function(){
	if(this.News.length<=0)return this.parseerror({"errcode":1,"errmsg":"no news to upload"});
	var news=[];
	for(var i=0;i<this.News.length;i++){
		news.push(["{",
		"\"thumb_media_id\":\"" + this.News[i].thumb_media_id + "\",",
		"\"author\":\"" + this.News[i].author + "\",",
		"\"title\":\"" + this.News[i].title + "\",",
		"\"content_source_url\":\"" + this.News[i].content_source_url + "\",",
		"\"content\":\"" + this.News[i].content + "\",",
		"\"digest\":\"" + this.News[i].digest + "\"",
		"}"].join(""));
	}
	return this.parseerror(MoLibHttpRequest.New(
		"https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=" + this.Auth.access_token,
		"POST",
		F.format("{\"articles\":[{0}]}",news.join(","))
	).getjson("utf-8"));
};
MoLibWXServicesMuti.prototype.uploadvideo = function(mediaid,title,description){
	return this.parseerror(MoLibHttpRequest.New(
		"https://api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token=" + this.Auth.access_token,
		"POST",
		F.format("{\"media_id\":\"{0}\",\"title\":\"{1}\",\"description\":\"{2}\"}",mediaid,F.jsEncode(title||""),F.jsEncode(description||""))
	).getjson("utf-8"));
};
MoLibWXServicesMuti.prototype.sendmessage = function(json){
	return this.parseerror(MoLibHttpRequest.New("https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=" + this.Auth.access_token,"POST",json).getjson("utf-8"));
};
MoLibWXServicesMuti.prototype.sendnews = function(mediaid){
	return this.sendmessage(F.format("{" + this.createfilter() + ",\"mpnews\":{\"media_id\":\"{0}\"},\"msgtype\":\"mpnews\"}",mediaid));
};
MoLibWXServicesMuti.prototype.sendtext = function(content){
	return this.sendmessage(F.format("{" + this.createfilter() + ",\"text\":{\"content\":\"{0}\"},\"msgtype\":\"text\"}",content));
};
MoLibWXServicesMuti.prototype.sendvoice= function(mediaid){
	return this.sendmessage(F.format("{" + this.createfilter() + ",\"voice\":{\"media_id\":\"{0}\"},\"msgtype\":\"voice\"}",mediaid));
};
MoLibWXServicesMuti.prototype.sendimage= function(mediaid){
	return this.sendmessage(F.format("{" + this.createfilter() + ",\"image\":{\"media_id\":\"{0}\"},\"msgtype\":\"image\"}",mediaid));
};
MoLibWXServicesMuti.prototype.sendvideo= function(mediaid,title,description){
	if(this.sendto=="group"){
		return this.sendmessage(F.format("{" + this.createfilter() + ",\"mpvideo\":{\"media_id\":\"{0}\"},\"msgtype\":\"mpvideo\"}",mediaid));
	}else{
		return this.sendmessage(F.format("{" + this.createfilter() + ",\"video\":{\"media_id\":\"{0}\",\"title\":\"{1}\",\"description\":\"{2}\"},\"msgtype\":\"video\"}",mediaid,title||"",description||""));
	}
};
MoLibWXServicesMuti.prototype.deletemsg = function(msgid){
	return this.parseerror(MoLibHttpRequest.New(
	"https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=" + this.Auth.access_token,
	"POST",
	"{\"msgid\":" + msgid + "}").getjson("utf-8"));	
};
</script>