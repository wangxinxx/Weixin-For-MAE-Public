<script language="jscript" runat="server">
function MoLibWXMessage(){
	this.SignManager={
		token:"",
		timestamp:"",
		nonce:"",
		comparewith:function(signature){
			Mo.Use("SHA1");
			F.sortable.clear();
			F.sortable.add(this.timestamp);
			F.sortable.add(this.nonce);
			F.sortable.add(this.token);
			F.sortable.sort(true);
			return MoLibSHA1.SHA1(F.sortable.join())==signature;
		}
	};
	this.News={
		length:0,
		push:function(value){
			this[this.length++]=value;
		},
		clear:function(){
			while(this.length>0){
				this[--this.length]=null;
			}
		},
		append:function(title, content, url, pic){
			this.push({"title":title, "content":content, "pic":pic||"", "url":url||""});
		}
	};
	this.MsgType="";
	this.ToUserName="";
	this.FromUserName="";
	this.CreateTime="";
	this.MsgId="";
	this.Request={};
}
MoLibWXMessage.New = function(){return new MoLibWXMessage();};
MoLibWXMessage.timespan = function(){
	var df = (new Date())-(new Date(1970,0,1));
	return (df-(df%1000))/1000-3600*8;
};
MoLibWXMessage.prototype.basic = function(){
	return {
		"MsgType":this.MsgType,
		"ToUserName":this.ToUserName,
		"FromUserName":this.FromUserName,
		"CreateTime":this.CreateTime,
		"MsgId":this.MsgId,
		"Request":this.Request
	}
};
MoLibWXMessage.prototype.loadRequest = function(requeststr){
	Mo.Use("XML");
	var xml = MoLibXML.Load(requeststr)
	if(xml.ROOT==null)return false;
	this.MsgType = xml.select("MsgType").text().toLowerCase();
	if(this.MsgType=="")return false;
	this.ToUserName = xml.select("ToUserName").text();
	this.FromUserName = xml.select("FromUserName").text();
	this.CreateTime = xml.select("CreateTime").text();
	if(this.MsgType!="event")this.MsgId = xml.select("MsgId").text();
	else{
		this.Request["Event"] = xml.select("Event").text();
		//subscribe,unsubscribe,SCAN,LOCATION,CLICK,VIEW,MASSSENDJOBFINISH
		if(this.Request["Event"]=="LOCATION"){
			this.Request["Latitude"] = xml.select("Latitude").text();
			this.Request["Longitude"] = xml.select("Longitude").text();
			this.Request["Precision"] = xml.select("Precision").text();
		}else if(this.Request["Event"]=="MASSSENDJOBFINISH"){
			this.Request["MsgID"] = xml.select("MsgID").text();
			this.Request["Status"] = xml.select("Status").text();
			this.Request["TotalCount"] = xml.select("TotalCount").text();
			this.Request["FilterCount"] = xml.select("FilterCount").text();
			this.Request["SentCount"] = xml.select("SentCount").text();
			this.Request["ErrorCount"] = xml.select("ErrorCount").text();
		}else{
			this.Request["EventKey"] = xml.select("EventKey").text();
			this.Request["Ticket"] = xml.select("Ticket").text();
		}
	}
	if(this.MsgType=="text"){
		this.Request["Content"] = xml.select("Content").text();
	}else if(this.MsgType=="image"){
		this.Request["PicUrl"] = xml.select("PicUrl").text();
		this.Request["MediaId"] = xml.select("MediaId").text();
	}else if(this.MsgType=="voice"){
		this.Request["Format"] = xml.select("Format").text();
		this.Request["MediaId"] = xml.select("MediaId").text();
		this.Request["Recognition"] = xml.select("Recognition").text();
	}else if(this.MsgType=="video"){
		this.Request["ThumbMediaId"] = xml.select("ThumbMediaId").text();
		this.Request["MediaId"] = xml.select("MediaId").text();
	}else if(this.MsgType=="location"){
		this.Request["Location_X"] = xml.select("Location_X").text();
		this.Request["Location_Y"] = xml.select("Location_Y").text();
		this.Request["Scale"] = xml.select("Scale").text();
		this.Request["Label"] = xml.select("Label").text();
	}else if(this.MsgType=="link"){
		this.Request["Title"] = xml.select("Title").text();
		this.Request["Description"] = xml.select("Description").text();
		this.Request["Url"] = xml.select("Url").text();
	}
	xml = null;
	return true
};
MoLibWXMessage.prototype.createTextResponse = function(content){
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[text]]></MsgType>",
	"<Content><![CDATA[" + content + "]]></Content>",
	"</xml>"].join("");
};
MoLibWXMessage.prototype.createImageResponse = function(mediaid){
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[image]]></MsgType>",
	"<Image><MediaId><![CDATA[" + mediaid + "]]></MediaId></Image>",
	"</xml>"].join("");
};
MoLibWXMessage.prototype.createVoiceResponse = function(mediaid){
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[voice]]></MsgType>",
	"<Voice><MediaId><![CDATA[" + mediaid + "]]></MediaId></Voice>",
	"</xml>"].join("");
};
MoLibWXMessage.prototype.createVideoResponse = function(mediaid,title,description){
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[video]]></MsgType>",
	"<Video><MediaId><![CDATA[" + mediaid + "]]></MediaId>"+
	(title!==undefined?"<Title><![CDATA[" + title + "]]></Title>" : "")+
	(description!==undefined?"<Description><![CDATA[" + description + "]]></Description>" : "")+
	"</Video>",
	"</xml>"].join("");
};
MoLibWXMessage.prototype.createMusicResponse = function(ThumbMediaId,title,description,MusicURL,HQMusicUrl){
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[music]]></MsgType>",
	"<Music>"+
	(title!==undefined?"<Title><![CDATA[" + title + "]]></Title>" : "")+
	(description!==undefined?"<Description><![CDATA[" + description + "]]></Description>" : "")+ 
	(MusicURL!==undefined?"<MusicUrl><![CDATA[" + MusicURL + "]]></MusicUrl>" : "")+ 
	(HQMusicUrl!==undefined?"<HQMusicUrl><![CDATA[" + HQMusicUrl + "]]></HQMusicUrl>" : "")+ 
	"<ThumbMediaId><![CDATA[" + ThumbMediaId + "]]></ThumbMediaId></Music>",
	"</xml>"].join("");
};
MoLibWXMessage.prototype.createNewsResponse = function(){
	if(arguments.length==4){
		this.News.clear();
		this.News.append(arguments[0],arguments[1],arguments[2],arguments[3]);
	}
	if(this.News.length<=0)return "";
	var news="";
	for(var i=0;i<this.News.length;i++){
		news+=["<item>",
		"<Title><![CDATA[" + this.News[i].title + "]]></Title>",
		"<Description><![CDATA[" + this.News[i].content + "]]></Description>",
		"<PicUrl><![CDATA[" + this.News[i].pic + "]]></PicUrl>",,
		"<Url><![CDATA[" + this.News[i].url + "]]></Url>",
		"</item>"].join("");
	}
	return ["<xml>",
	"<ToUserName><![CDATA[" + this.FromUserName + "]]></ToUserName>",
	"<FromUserName><![CDATA[" + this.ToUserName + "]]></FromUserName>",
	"<CreateTime>" + MoLibWXMessage.timespan() + "</CreateTime>",
	"<MsgType><![CDATA[news]]></MsgType>",
	"<ArticleCount>" + this.News.length + "</ArticleCount>",
	"<Articles>",
	news,
	"</Articles>",
	"<FuncFlag>0</FuncFlag>",
	"</xml>"].join("");
};
</script>