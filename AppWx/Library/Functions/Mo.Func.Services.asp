﻿<%
class MoFuncServices
	dim fromdefault
	 Private Sub Class_Initialize()
		fromdefault = false
	 End Sub
	 public default function callee__(Byref WX,Byref System)
	 	callee__=""
	 	dim L:set L = Model__("Subscriber","Id").where("OpenId='" & WX.FromUserName & "'").query().fetch()
	 	if not L.Eof() then
	 		dim R:set R = L.Read()
	 		if R.Mode<>"none" then
	 			fromdefault = true
				if R.Mode="mode_md5" then callee__ = HashMD5(WX,System)
				if R.Mode="mode_location" then callee__ = GetLocationByIp(WX,System)
				if R.Mode="mode_whois" then callee__ = Mo("Public:Func.Whois")(WX,System)
				if R.Mode="mode_qrcode" then callee__ = QRCodeNews(WX,System)
			end if
		end if
	 end function

	'获取指定文本的二维码图片，模糊匹配规则为：^qr(code)?(\s+)([\s\S]+?)$
	 public function QRCodeNews(Byref WX,Byref System)
	 	dim content : content = ReplaceEx(WX.Request.Content,"^qr(code)?(\s+)([\s\S]+?)$","$3")
	 	if fromdefault then content = WX.Request.Content
	 	if len(content)<=50 and len(content)>0 then
	 		Mo.Use "HttpRequest"
	 		dim path:path = MD5(content) & ".jpg"
	 		MoLibHttpRequest.New("http://www.thinkasp.cn/qrcode/create/","POST","format=Jpeg&scale=4&src=" & F.encode(content),false)_
	 		.saveToFile F.mappath("/Contents/Medias/Qrcode/" & path)
	 		if F.fso.FileExists(F.mappath("/Contents/Medias/Qrcode/" & path)) then
	 			QRCodeNews = WX.createNewsResponse("成功生成二维码",_
	 			"二维码内容：" & content,_
	 			"http://wx.thinkasp.cn/Contents/Medias/Qrcode/show.html?q=" & F.encode(path) & "&c=" & F.encode(content),_
	 			"http://wx.thinkasp.cn/Contents/Medias/Qrcode/" & path)
	 		end if
		end if
	 end function

	'获取指定文本的MD5值，模糊匹配规则为：^md5(\s+)(.+?)$
	 public function HashMD5(Byref WX,Byref System)
	 	dim content : content = ReplaceEx(WX.Request.Content,"^md5(\s+)(.+?)$","$2")
	 	if fromdefault then content = WX.Request.Content
		HashMD5 = WX.createTextResponse("[" & content & "]的MD5值：" & F.md5(content))
	 end function
	 
	'获取指定Ip或主机名的地理位置，模糊匹配规则为：^location(\s+)(.+?)$
	 public function GetLocationByIp(Byref WX,Byref System)
	 	dim ip:ip = ReplaceEx(WX.Request.Content,"^location(\s+)(.+?)$","$2")
	 	if fromdefault then ip = WX.Request.Content
		Mo.Use "HttpRequest"
		dim json:set json = MoLibHttpRequest.New("http://www.thinkasp.cn/home/location/ip/" & ip,"GET","",false).getjson("utf-8")
		GetLocationByIp = WX.createTextResponse("[" & ip & "]的地理位置：" & vbcrlf & json.Location)
	 end function
	 
	 '查询成语，模糊匹配规则为：^cy(\s+)(.+?)$
	 public function GetChengyu(Byref WX,Byref System)
	 	dim cy:cy = ReplaceEx(WX.Request.Content,"^cy(\s+)(.+?)$","$2")
	 	if fromdefault then cy = WX.Request.Content
		dim L,responsestr : set L = Model__("cy","ID","Sqlite","mo_").where("name like '" & F.safe(cy,40) & "%'").query().fetch()
		if L.Eof() then
			responsestr = "没有查询到成语[" & cy & "]的详细内容。"
		else
			dim R:set R = L.Read()
			responsestr = "【" & cy & "】：" & vbcrlf
			responsestr = responsestr & R.getter__("spell") & vbcrlf
			responsestr = responsestr & R.getter__("content") & vbcrlf
			responsestr = responsestr & R.getter__("derivation")
		end if
		GetChengyu = WX.createTextResponse(responsestr)
	 end function
	 
	 '随机发送笑话，可自定义匹配规则，无限制
	 public function GetXiaohua(Byref WX,Byref System)
		dim L,responsestr,content : set L = Model__("xiaohua","articleid","Sqlite","mo_").limit(1,1).orderby("RANDOM()").query().fetch()
		if not L.Eof() then
			dim R:set R = L.Read()
			content = R.getter__("content")
			content = replaceex(content,">(\s+)<","><")
			content = replaceex(content,"<br(.*?)>",vbcrlf)
			content = replace(content,"&nbsp;","")
			content = replaceex(content,"(\r\n){2,}",vbcrlf)
			responsestr = "【" & R.getter__("title") & "】：" & vbcrlf
			responsestr = responsestr & content & vbcrlf
		end if
		GetXiaohua = WX.createTextResponse(responsestr)
	 end function
End Class
%>