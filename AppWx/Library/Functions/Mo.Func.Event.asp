﻿<%
class MoFuncEvent
	 Private Sub Class_Initialize()
		'your code
	 End Sub
	 '帮助
	 public default function callee__(Byref WX,Byref System)
	 	dim helpcontext:helpcontext="功能：" & vbcrlf
	 	helpcontext = helpcontext & "  1、whois 域名，查询域名WHOIS" & vbcrlf
	 	helpcontext = helpcontext & "  2、location IP/域名，查询主机地理位置" & vbcrlf
	 	helpcontext = helpcontext & "  3、md5 字符，计算MD5摘要" & vbcrlf
	 	helpcontext = helpcontext & "  4、qr 字符，获取指定文本的二维码" & vbcrlf
	 	helpcontext = helpcontext & "同时，可以通过菜单设置当前对话模式，就可以不用输入前缀，直接使用相关功能。重置对话模式则返回原功能使用方法。"
		callee__=WX.createTextResponse(helpcontext)
	 end function

	 '设置模式
	 public function SetMode(Byref WX,Byref System)
	 	if WX.Request.EventKey="mode_reset" then WX.Request.EventKey="none"
	 	Model__("Subscriber","Id").where("OpenId='" & WX.FromUserName & "'").update "Mode",WX.Request.EventKey
		if WX.Request.EventKey="mode_md5" then SetMode = WX.createTextResponse("已成功设置当前对话模式为MD5，将根据您的输入计算MD5摘要。")
		if WX.Request.EventKey="mode_whois" then SetMode = WX.createTextResponse("已成功设置当前对话模式为WHOIS，将根据您的输入查询域名WHOIS信息。")
		if WX.Request.EventKey="mode_location" then SetMode = WX.createTextResponse("已成功设置当前对话模式为IP转地理位置，将尝试获取您的输入IP对应的地理位置。")
		if WX.Request.EventKey="mode_qrcode" then SetMode = WX.createTextResponse("已成功设置当前对话模式为获取二维码，将尝试获取您输入内容的二维码。")
		if WX.Request.EventKey="none" then SetMode = WX.createTextResponse("已成功重置对话模式。")
	 end function
End Class
%>