﻿<%
class MoFuncWhois
	Private Sub Class_Initialize()
		'your code
	End Sub
	
	'获取指定域名的WHOIS，模糊匹配规则为：^whois(\s+)(.+?)$
	public default function callee__(byref WX,Byref System)
		dim convert:set convert = F.activex("dev.mo.convert")
		dim domain:domain = ReplaceEx(WX.Request.Content,"^whois(\s+)(.+?)$","$2")
		dim result: result = convert.GetMyWhois(domain,F.MapPath("/Contents/whois.config.xml"),false)
		result = actinfor(result,domain)
		callee__ = WX.createTextResponse(result)
	end function
	private function actinfor(byval str,byval domain)
		dim start,dmname,jsonstr
		str = replace(str,vbcr,"")
		str = replaceex(str,"^\s+","")
		start=str
		dmname = getKeyValue(start,"Domain Name")
		if instr(dmname,", ") > 0 then dmname = split(dmname,", ")(0)
		if left(lcase(dmname),4)="xn--" then dm1="(" & domain & ")"
		jsonstr = jsonstr & "域名: " & vbcrlf & dmname & dm1 & vbcrlf
		jsonstr = jsonstr & "注册商: " & vbcrlf & getKeyValue1(start,"Registrar|Sponsoring Registrar|Registrar Name") & vbcrlf
		jsonstr = jsonstr & "DNS服务器: " & vbcrlf & getKeyValue1(start,"Name Server|Name Server1|Name Server2") & vbcrlf
		jsonstr = jsonstr & "状态: " & vbcrlf & getKeyValue(start,"Status") & vbcrlf
		jsonstr = jsonstr & "注册时间: " & vbcrlf & getKeyValue1(start,"Created On|Creation Date|Registration Date|Create Date") & vbcrlf
		jsonstr = jsonstr & "到期时间: " & vbcrlf & getKeyValue(start,"Expiration Date") & vbcrlf
		jsonstr = jsonstr & "注册组织: " & vbcrlf & getKeyValue1(start,"Registrant Organization|Registrant ORG|Registrant") & vbcrlf
		jsonstr = jsonstr & "注册人: " & vbcrlf & getKeyValue(start,"Registrant Name") & vbcrlf
		jsonstr = jsonstr & "邮箱: " & vbcrlf & getKeyValue1(start,"Registrant Email|Registrant Contact Email")
		actinfor = jsonstr
	end function
	private function getKeyValue1(byval source,byval key)
		dim list,i
		i=0
		list = split(key,"|")
		for i=0 to ubound(list)
			getKeyValue1 = getKeyValue1 & (getKeyValue(source,list(i))) & " "
		next
		getKeyValue1 = trim(getKeyValue1)
	end function

	private function getKeyValue(byval source,byval key)
		dim m,result,ms
		result=""
		set m = getMatch(source,"(^|\n(\s+)?|domain )" & replace(key,"-","\-") & "\b\:(.+)")
		for each ms in m
			if trim(ms.submatches(2))<>"" then result = result & trim(ms.submatches(2)) & ", "
		next 
		if result<>"" then result = left(result,len(result)-2)
		getKeyValue = result
	end function
End Class
%>