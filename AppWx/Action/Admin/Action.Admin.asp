﻿<%
class ActionAdmin
	private LOGPATH,WX,System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&error=1&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if not L.Eof() then
				set System = L.Read()
				Mo.assign "subtitle","管理首页"
			end if
		end if
	end sub
	public sub index
		dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
		if not L.Eof() then
			if is_empty(System.Token) then System.Token = Rndstr(30)
			Mo.assign "System",System
		end if
		Mo.assign "NotifyUrl","http://" & F.server("HTTP_HOST") & MO_ROOT & "?m=Notice"
		Mo.display "Setting"
	end sub
	public sub savesetting
		'注意，这里没做数据验证
		F.post "Save_Funs_Msg",F.post.int("Save_Funs_Msg",0)
		if not Model__("System","Id").where("Tag=1").query().fetch().Eof() then
			F.post.remove "Tag"
			Model__("System","Id").where("Tag=1").update()
			F.goto "?m=Admin&error=0&msg=" & F.encode("保存成功")
		else
			F.goto "?m=Admin&error=1&msg=" & F.encode("记录不存在")
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>