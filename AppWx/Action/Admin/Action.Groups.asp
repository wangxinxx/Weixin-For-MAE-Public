﻿<%
class ActionGroups
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&error=1&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","粉丝分组管理"
			end if
		end if
	end sub
	public sub index
		Model__("Groups","Id")_
		.orderby("GroupId asc")_
		.query()_
		.assign "Groups"
		Mo.display "Admin:Groups"
	end sub
	public sub save
		dim FV,WX,json
		Set FV = Mo("FormValidatee")
		FV.AddRule "Name","required;max-length:7","名称必填，不能超过7个汉字"
		if F.post.int("GroupId",0)>0 then
			If FV.Validate() Then
				if not Model__("Groups","Id").where("Name='" & F.post.safe("Name",7) & "' and GroupId<>" & F.post.int("GroupId",0)).query().fetch().Eof() then
					F.goto "?m=Groups&error=1&msg=" & F.encode("已存在相同名称的分组")
				else
					Mo.Use "WXServices"
					set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
					if WX.getAccessToken() then
						set json = WX.updategroup(F.post.int("GroupId",0),F.post.safe("Name",7))
						if json.error then
							F.goto "?m=Groups",WX.errmsg
						else
							Model__("Groups","Id").where("GroupId=" & F.post.int("GroupId",0)).update "Name",F.post.safe("Name",7)
							F.goto "?m=Groups&error=0&msg=" & F.encode("分组修改成功。")
						end if
					else
						F.goto "?m=Groups&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
					end if
				end if
			else
				F.goto "?m=Groups&error=1&msg=" & F.encode("修改失败。" & vbcrlf & FV.exception)
			end if
		else
			If FV.Validate() Then
				if not Model__("Groups","Id").where("Name='" & F.post.safe("Name",7) & "'").query().fetch().Eof() then
					F.goto "?m=Groups&error=1&msg=" & F.encode("已存在相同名称的分组")
				else
					Mo.Use "WXServices"
					set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
					if WX.getAccessToken() then
						set json = WX.creategroup(F.post.safe("Name",7))
						if json.error then
							F.goto "?m=Groups",WX.errmsg
						else
							Model__("Groups","Id").insert "Name",json.group.name,"GroupId",json.group.id,"SrcId",System.Wx_Acount_SrcId
							F.goto "?m=Groups&error=0&msg=" & F.encode("分组创建成功。")
						end if
					else
						F.goto "?m=Groups&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
					end if
				end if
			else
				F.goto "?m=Groups&error=1&msg=" & F.encode("创建失败。" & vbcrlf & FV.exception)
			end if
		end if
	end sub
	public sub [get]
		Mo.Use "WXServices"
		dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			set json = WX.getgroups()
			if json.error then
				F.goto "?m=Groups",WX.errmsg
			else
				Model__("Groups","Id").delete true
				F.foreach json.groups,GetRef("EnumGroups"),System
				F.goto "?m=Groups&error=0&msg=" & F.encode("分组获取成功。")
			end if
		else
			F.goto "?m=Groups&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
function EnumGroups(i,v,o,System)
	Model__("Groups","Id").insert "Name",v.name,"GroupId",v.id,"SubCount",v.count,"SrcId",System.Wx_Acount_SrcId
end function
%>