﻿<%
class ActionMediaMuti
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","群发素材管理"
			end if
		end if
	end sub
	public sub index
		Model__("MediaMuti","Id").limit(F.get.int("page",1),10).orderby("Id desc").query().assign "MediaMuti"
		Model__("Media","Id").where("Type='video'").orderby("Id desc").query().assign "MediaVideo"
		Model__("Media","Id").where("Type='thumb'").orderby("Id desc").query().assign "MediaThumb"
		Mo.display "Admin:MediaMuti"
	end sub
	public sub delete
		Model__("MediaMuti","Id").where("Id=" & F.get.int("Id",0)).delete()
		F.goto "?m=MediaMuti&error=0&msg=" & F.encode("操作成功")
	end sub
	public sub save
		Mo.Use "WXServicesMuti"
		dim WX,json,jsonsrc : set WX = MoLibWXServicesMuti.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			if F.post("Type")="video" then
				set json = WX.uploadvideo(F.post("MediaId"),F.post("Title"),F.post("Description"))
				if json.error then
					F.goto "?m=MediaMuti&error=1&msg=" & F.encode(WX.errmsg)
				else
					Mo.Use "JsonGenerater"
					set jsonsrc = MoLibJsonGenerater.New()
					jsonsrc.put "Title",F.post("Title")
					jsonsrc.put "Description",F.post("Description")
					Model__("MediaMuti","Id").insert "Name",F.post.safe("Title"),"Type",json.type, "MediaId",json.media_id,"CreatedAt",json.created_at,"Details",jsonsrc.toString()
					F.goto "?m=MediaMuti&error=0&msg=" & F.encode("成功保存。")
				end if
			else
				WX.News.Clear()
				WX.News.append F.post("News_MediaId"),F.post("News_Title"),F.post("News_Content"),F.post("News_Content_Source_Url"),F.post("News_Author"),F.post("News_Digest")
				set json = WX.uploadnews()
				if json.error then
					F.goto "?m=MediaMuti",WX.errmsg
				else
					Mo.Use "JsonGenerater"
					set jsonsrc = MoLibJsonGenerater.New()
					jsonsrc.put "Title",F.post("News_Title")
					jsonsrc.put "Content",F.post("News_Content")
					jsonsrc.put "Content_Source_Url",F.post("News_Content_Source_Url")
					jsonsrc.put "Author",F.post("News_Author")
					jsonsrc.put "Digest",F.post("News_Digest")
					Model__("MediaMuti","Id").insert "Name",F.post.safe("News_Title"),"Type",json.type, "MediaId",json.media_id,"CreatedAt",json.created_at,"Details",jsonsrc.toString()
					F.goto "?m=MediaMuti&error=0&msg=" & F.encode("成功保存。")
				end if
			end if
		else
			F.goto "?m=MediaMuti&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
function ParseMessage(msg)
	ParseMessage = F.json(msg).Title
end function
%>