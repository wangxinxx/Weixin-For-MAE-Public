﻿<%
class ActionResponseRules
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","自动回复管理"
			end if
		end if
	end sub
	public sub index
		Model__("ResponseRules","Id").limit(F.get.int("page",1),15).orderby("Id desc").query().assign "Rules"
		Mo.display "Admin:ResponseRules"
	end sub
	public sub edit
		dim Id:Id = F.get.int("Id",0)
		if Id>0 then Model__("ResponseRules","Id").where("Id=" & Id).query().assign()
		Model__("MediaMessages","Id").orderby("Id desc").query().assign "Messages"
		Model__("Menus","Id").cname("A").where("A.Type='click' and (select count(*) from Wx_Menus B where B.ParentTag=A.Tag)<=0").orderby("A.OrderBy asc,A.Id desc").query().assign "Menus"
		dim files,file,content
		dim L,Matches,Match,fntype,i : set L = Mo___()
		fntype="自定义功能--"
		set files = F.fso.getfolder(F.mappath(MO_APP & "Library/Functions")).files
		for each file in files
			if F.string.startWith(file.name,"Mo.Func.") then
				Mo.Use "File"
				content = MoLibFile.readtext(F.mappath(MO_APP & "Library/Functions/" & file.name))
				set Matches = GetMatch(content,"\s(\'(.+?)(\s+))?public(\s+)default(\s+)function(\s+)(\w+)(\s*)\(Byref(\s+)WX(\s*),(\s*)Byref(\s+)System\)")
				for each Match in Matches
					L.AddNew()
					L.Set "Name",fntype & split(file.name,".")(2)
					L.Set "Key","Functions:Func." & split(file.name,".")(2)
					L.Set "Comment",Match.submatches(1)
				next
				set Matches = GetMatch(content,"\s(\'(.+?)(\s+))?public(\s+)function(\s+)(\w+)(\s*)\(Byref(\s+)WX(\s*),(\s*)Byref(\s+)System\)")
				for each Match in Matches
					L.AddNew()
					L.Set "Name",fntype & split(file.name,".")(2) & "." & Match.submatches(5)
					L.Set "Key","Functions:Func." & split(file.name,".")(2) & "." & Match.submatches(5)
					L.Set "Comment",Match.submatches(1)
				next
			end if
		next
		Mo.assign "Functions",L
		Mo.assign "Id",Id
		Mo.display "Admin:ResponseRulesEdit"
	end sub
	public sub disable
		dim state:state = F.get.int("state",0)
		if state<>0 and state<>1 then state=1
		Model__("ResponseRules","Id").where("Id=" & F.get.int("Id",0)).update "Disabled",state
		F.goto "?m=ResponseRules&error=0&msg=" & F.encode("操作成功")
	end sub
	public sub saverules
		F.post "Name",F.post.safe("Name",30)
		dim Id:Id = F.post.int("Id",0)
		If Id>0 then
			F.post "ModifyAt",F.timespan()
			Model__("ResponseRules","Id").where("Id=" & Id).update()
		else
			F.post "CreateAt",F.timespan()
			F.post "ModifyAt",F.timespan()
			Model__("ResponseRules","Id").insert()
		end if
		F.echo "规则已保存。"
	end sub
	public sub delete
		Model__("ResponseRules","Id").where("Id=" & F.get.int("Id",0)).delete()
		F.goto "?m=ResponseRules&error=0&msg=" & F.encode("规则已删除")
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>