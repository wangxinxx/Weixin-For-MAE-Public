﻿<%
class ActionLogin
	public sub index
		if F.server("REQUEST_METHOD")="POST" then
			if F.post.safe("login.code")="" or F.session("wx_session_code")="" or lcase(F.post.safe("login.code"))<>lcase(F.session("wx_session_code")) then
				F.goto "?m=login&msg=" & F.encode("请输入正确的验证码")
				exit sub
			end if
			F.session "wx_session_code",null
			dim name,pwd,code
			name = F.post.exp("login.name","^\w+$")
			pwd = F.post("login.pwd")
			code = F.post.safe("login.code")
			if name="" then
				F.goto "?m=login&msg=" & F.encode("请输入合法的用户名")
				exit sub
			end if
			dim L : set L = Model__("Authorize","Id").where("Name='" & name & "'").query().fetch()
			if L.Eof() then
				WriteOperateLog "不存在的用户","失败",name
				F.goto "?m=login&msg=" & F.encode("错误的登录信息")
			else
				dim R : set R = L.Read()
				Mo.Use "SHA1"
				if MoLibSHA1.SHA1(F.md5(MoLibSHA1.SHA2(pwd)))<>R.Auth then
					WriteOperateLog "登录密码错误","失败",name
					F.goto "?m=login&msg=" & F.encode("错误的登录信息")
				else
					if R.IsLocked = 1 then
						WriteOperateLog "账户被锁定。原因：" & R.LockReason,"失败",name
						F.goto "?m=login&msg=" & F.encode("账户被锁定。原因：" & R.LockReason)
					else
						Model__("Authorize","Id").where("Name='" & name & "'").update "LoginTimes",R.LoginTimes+1,"LastLoginDate",F.timespan(),"LastLoginIP",F.server("REMOTE_ADDR")
						F.session "wx_auth_name",R.Name
						F.session "wx_auth_LastLoginDate",R.LastLoginDate
						F.session "wx_auth_LastLoginIP",R.LastLoginIP
						F.session "wx_auth_LoginTimes",R.LoginTimes+1
						F.session "wx_access_flag",R.AccessFlag
						WriteOperateLog "用户登录成功。","成功",""
						'Model__.debug()
						F.redirect "?m=Admin"
					end if
				end if
			end if
		else
			Mo.display "Admin:login"
		end if
	end sub
	public sub logout
		if not is_empty(F.session("wx_auth_name")) then
			WriteOperateLog "用户登出。","成功",""
			F.session "wx_auth_name",""
			F.session "wx_auth_LastLoginDate",""
			F.session "wx_auth_LastLoginIP",""
			F.session "wx_auth_LoginTimes",""
			F.session "wx_access_flag",""
			F.session "Id",""
		end if
		F.goto "?m=login&msg=" & F.encode("已退出")
	end sub
	public sub code
		Mo("SafeCode").Code "wx_session_code" 
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>