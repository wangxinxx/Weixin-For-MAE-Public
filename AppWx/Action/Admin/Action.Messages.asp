﻿<%
class ActionMessages
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","消息管理"
				Mo.assign "timespan48",F.timespan()-48*3600
			end if
		end if
	end sub
	public sub index
		dim sqlwhere,tpl,LastTimeLine,sqlwhere2:sqlwhere="1=1"
		tpl = "Messages"
		if F.get("for") = "ajax" then tpl = "MessagesList"
		if F.get.exp("OpenId","^([\w\.\-]+)$")<>"" then
			sqlwhere = " (ToUserName='" & F.get("OpenId") & "' or FromUserName='" & F.get("OpenId") & "')"
		end if
		if F.get.int("LastTimeLine",0)>0 then
			sqlwhere2 = sqlwhere & " and CreateTime>" & F.get.int("LastTimeLine",0)
			if Model__("Messages","Id").select("Id").where(sqlwhere2).query().fetch().Eof() then exit sub
		end if
		LastTimeLine = Model__("Messages","Id")_
		.where(sqlwhere)_
		.limit(F.get.int("page",1),15)_
		.orderby("CreateTime desc,Id desc")_
		.query()_
		.assign("Messages")_
		.read("CreateTime")
		if LastTimeLine="" then LastTimeLine=0
		Mo.assign "OpenId",F.get.exp("OpenId","^([\w\.\-]+)$")
		Mo.assign "Page",F.get.int("page",1)
		Mo.assign "LastTimeLine",LastTimeLine
		if F.get("for") = "ajax" then F.echo LastTimeLine & "|"
		if not (F.get("for") = "ajax" and Mo.Value("Messages").recordcount=0) then Mo.display "Admin:" & tpl
	end sub
	public sub getaddressbygps
		dim location:location = F.get.exp("location","^([\d\,\.]+)$")
		if location="" then
			F.echo "坐标错误"
			exit sub
		end if
		dim usecache,atend:usecache=true
		atend=false
		dim L:set L = Model__("Location","Id").where("Location='" & location & "'").query().fetch()
		dim Address
		if not L.Eof() then Address = L.Read("Address") else atend=true
		if Address<>"" then
			F.echo "LOCATION(" & Address & ")"
		else
			usecache = false
		end if
		if not is_empty(System.Tx_key) and not usecache then
			Mo.Use "TXAPI"
			dim TXAPI : set TXAPI = MoLibTXAPI.New(System.Tx_key)
			dim Gps : set Gps = TXAPI.Translate(location)
			dim Pos : set Pos = TXAPI.Geoencoder(Gps.x & "," & Gps.y)
			if Pos.status=0 then
				F.echo "LOCATION(" & Pos.result.address & ")"
				if atend then 
					Model__("Location","Id").insert "Location",location,"Address",Pos.result.address
				else
					Model__("Location","Id").where("Location='" & location & "'").update "Address",Pos.result.address
				end if
			else
				F.echo "坐标解析失败"
			end if
		end if
	end sub
	public sub showdetail
		dim details:details = Model__("Messages","Id").where("Id=" & F.get.int("Id",0)).query().fetch().Read("Details")
		Mo.Use "JsonParser"
		F.echo "<div class=""detial""><textarea style=""width:717px;height:250px;"">" & MoLibJsonParser.unParse(MoLibJsonParser.parse(details),"  ") & "</textarea></div>"
	end sub
	public sub delete
		Model__("Messages","Id").where("Id in(" & F.get.intlist("Id",0) & ")").delete()
		F.goto "?m=Messages&page=" & F.get.int("page",1) & "&error=0&msg=" & F.encode("操作成功，影响记录数(" & Model__.lastRows & ")。")
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>