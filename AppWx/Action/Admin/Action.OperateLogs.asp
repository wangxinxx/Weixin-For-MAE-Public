﻿<%
class ActionOperateLogs
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				Mo.assign "System",F.json("{""SystemName"":""管理系统""}")
			else
				set System = L.Read()
				Mo.assign "System",System
			end if
			Mo.assign "subtitle","操作日志"
		end if
	end sub
	public sub index
		dim sqlwhere:sqlwhere=""
		if F.session("wx_access_flag")="0" then
			sqlwhere = "1=1"
			if F.get.exp("username","^\w+$")<>"" then sqlwhere = "[UserName]='" & F.get.exp("username","^\w+$") & "'"
		else
			sqlwhere = "[UserName]='" & F.session("wx_auth_name") & "'"
		end if
		Model__("OperateLog","Id").where(sqlwhere).orderby("Id desc").limit(F.get.int("page",1),15).query().assign "OperateLogs"
		Mo.display "Admin:OperateLogs"
	end sub
	public sub delete
		dim sqlwhere:sqlwhere=""
		if F.session("wx_access_flag")="0" then
			sqlwhere = "1=1"
		else
			F.goto "?m=OperateLogs&page=" & F.get.int("page",1) & "&error=1&msg=" & F.encode("您没有权限删除日志。")
			exit sub
		end if
		Model__("OperateLog","Id").where("Id in(" & F.get.intlist("Id",0) & ")").delete()
		F.goto "?m=OperateLogs&page=" & F.get.int("page",1) & "&error=0&msg=" & F.encode("操作成功，影响记录数(" & Model__.lastRows & ")。")
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>