﻿<%
class ActionMenus
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","自定义菜单管理"
			end if
		end if
	end sub
	public sub index
		dim ptag : ptag = F.get.exp("Tag","^\w+$")
		dim sqlwhere : sqlwere = "ParentTag='" & ptag & "'"
		if ptag="" then sqlwere = "(ParentTag='' or ParentTag is null)"
		if ptag<>"" then Mo.assign "SubMunu","&gt;&gt;" & Model__("Menus","Id").where("Tag='" & ptag & "'").query().Read("Name")
		dim L : set L = Model__("Menus","Id")_
		.where(sqlwere)_
		.orderby("Orderby asc,Id desc")_
		.query()_
		.fetch()
		if ptag="" and L.recordcount>=3 or ptag<>"" and L.recordcount>=5 then Mo.assign "CanNotAdd","yes"
		Mo.assign "Menus",L
		Mo.display "Admin:Menus"
	end sub
	public sub deletemenu
		Mo.Use "WXServices"
		dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			dim json1:set json1 = WX.deletemenu()
			if json1.error then
				F.goto "?m=Menus&error=1&msg=" & F.encode(WX.errmsg)
			else
				F.goto "?m=Menus&error=0&msg=" & F.encode("菜单成功删除。")
			end if
		else
			F.goto "?m=Menus&error=1&msg=" & F.encode("Token获取失败" & WX.errmsg)
		end if
	end sub
	public sub import
		Mo.assign "action","import"
		Mo.Use "WXServices"
		dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			if F.get("preview")="yes" then
				Mo.Use "JsonParser"
				Mo.assign "Preview",MoLibJsonParser.unParse(F.json(WX.getmenu()),"  ")
			else
				dim json : set json = F.json(WX.getmenu())
				if not isnull(json) then
					if F.has(json,"menu") then
						dim menu,menu1,m_tag,m_order,i,j
						m_order = 1
						on error resume next
						if json.menu.button.length>0 then
							Model__.ShiwuKaishi()
							Model__("Menus","Id").delete true
							for i=0 to json.menu.button.length-1
								m_tag = "menu" & m_order
								set menu = json.menu.button.getter__(i)
								Model__("Menus","Id").insert _
								"Name",menu.getter__("name"),_
								"Type", menu.getter__("type"), "Key",menu.getter__("key"), "Url",menu.getter__("url"), "ParentTag","", "Tag",m_tag, "Orderby",m_order
								m_order = m_order + 1
								if menu.sub_button.length>0 then
									for j=0 to menu.sub_button.length-1
										set menu1 = menu.sub_button.getter__(j)
										Model__("Menus","Id").insert _
										"Name",menu1.getter__("name"),_
										"Type",menu1.getter__("type"),_
										"Key",menu1.getter__("key"), "Url",menu1.getter__("url"), "ParentTag",m_tag, "Tag","menu" & m_order, "Orderby",m_order
										m_order = m_order + 1
									next
								end if
							next
							if not err then
								Model__.ShiwuTijiao()
								F.goto "?m=Menus&error=0&msg=" & F.encode("菜单成功导入")
							else
								Model__.ShiwuHuigun()
								Mo.assign "Preview",err.description
							end if
						else
							Mo.assign "Preview","没有要导入的菜单"
						end if
					else
						Mo.assign "Preview","菜单数据错误"
					end if
				else
					Mo.assign "Preview","菜单数据获取失败"
				end if
			end if
		else
			Mo.assign "Preview&error=1&msg=" & F.encode("Token获取失败" & WX.errmsg)
		end if
		Mo.display "Admin:Menus_View"
	end sub
	public sub create
		Mo.Use "JsonGenerater"
		dim json : set json = MoLibJsonGenerater.New()
		json.pushAsArray "button"
		dim i,L,R,L1,K ,R1: set L = Model__("Menus","Id").where("(ParentTag='' or ParentTag is null)").orderby("Orderby asc,Id desc").query().fetch()
		i=0
		do while not L.Eof()
			set R = L.Read()
			json.pushAsObject "button[" & i & "]"
			set L1 = Model__("Menus","Id").where("ParentTag='" & R.Tag & "'").orderby("Orderby asc,Id desc").query().fetch()
			if L1.Eof() then
				json.push "button[" & i & "].type",R.Type
				json.push "button[" & i & "].name",R.Name
				if R.Type="click" then json.push "button[" & i & "].key",R.Key
				if R.Type="view" then json.push "button[" & i & "].url",R.Url
			else
				k=0
				json.push "button[" & i & "].name",R.Name
				json.pushAsArray "button[" & i & "].sub_button"
				do while not L1.Eof()
					set R1 = L1.Read()
					json.pushAsObject "button[" & i & "].sub_button[" & k & "]"
					json.push "button[" & i & "].sub_button[" & k & "].type",R1.Type
					json.push "button[" & i & "].sub_button[" & k & "].name",R1.Name
					if R1.Type="click" then json.push "button[" & i & "].sub_button[" & k & "].key",R1.Key
					if R1.Type="view" then json.push "button[" & i & "].sub_button[" & k & "].url",R1.Url
					k=k+1
				loop
			end if
			i=i+1
		loop
		if F.get("preview")="yes" then
			Mo.assign "Preview",MoLibJsonParser.unParse(json.table,"  ")
			Mo.display "Admin:Menus_View"
		else
			Mo.Use "WXServices"
			dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
			if WX.getAccessToken() then
				dim json1:set json1 = WX.createmenu(MoLibJsonParser.unParse(json.table,vbtab))
				if json1.error then
					F.goto "?m=Menus&a=create&preview=yes&error=1&msg=" & F.encode(WX.errmsg)
				else
					F.goto "?m=Menus&error=0&msg=" & F.encode("菜单成功创建。")
				end if
			else
				F.goto "?m=Menus&a=create&preview=yes&error=1&msg=" & F.encode("Token获取失败" & WX.errmsg)
			end if
		end if
	end sub
	public sub save
		F.post "OrderBy",F.post.int("OrderBy",1)
		dim FV
		Set FV = Mo("FormValidatee")
		FV.AddRule "Name","required;max-length:7","名称必填，不能超过7个汉字"
		FV.AddRule "Type","required;exp:/^(click|view)$/igm","类型错误，只能为click或view"
		if F.post("Type")="click" then FV.AddRule "Key","required;exp:/^(\w+)$/igm","标示错误，只能是数字和下划线的组合"
		if F.post("Type")="view" then FV.AddRule "Url","required;","URL必填"
		if F.post.int("Id",0)>0 then
			If FV.Validate() Then
				if F.post("Type")="click" and not Model__("Menus","Id").where("Type='click' and Key='" & F.post.exp("Key","^\w+$") & "' and Id<>" & F.post.int("Id",0)).query().fetch().Eof() then
					F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=1&msg=" & F.encode("已存在相同标示的记录")
				else
					Model__("Menus","Id").where("Id=" & F.post.int("Id",0)).update()
					F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=0&msg=" & F.encode("修改成功")
				end if
			else
				F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=1&msg=" & F.encode("修改失败。" & vbcrlf & FV.exception)
			end if
		else
			FV.AddRule "Tag","required;exp:/^(\w+)$/igm","标签错误，只能是数字和下划线的组合"
			If FV.Validate() Then
				if F.post("Type")="click" and not Model__("Menus","Id").where("Type='click' and Key='" & F.post.exp("Key","^\w+$") & "'").query().fetch().Eof() then
					F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=1&msg=" & F.encode("已存在相同标示的记录")
				elseif not Model__("Menus","Id").where("Tag='" & F.post.exp("Tag","^\w+$") & "'").query().fetch().Eof() then
					F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=1&msg=" & F.encode("已存在相同标签的记录")
				else
					Model__("Menus","Id").insert()
					F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=0&msg=" & F.encode("添加成功")
				end if
			else
				F.goto "?m=Menus&Tag=" & F.post("ParentTag") & "&error=1&msg=" & F.encode("添加失败。" & vbcrlf & FV.exception)
			end if
		end if
	end sub
	public sub delete
		dim Tag,L:Tag = F.get.exp("Tag","^(\w+)$")
		set L = Model__("Menus","Id").where("ParentTag='" & Tag & "'").query().fetch()
		if L.Eof() then
			Model__("Menus","Id").where("Tag='" & Tag & "'").delete()
			F.goto "?m=Menus&error=0&msg=" & F.encode("操作成功。")
		else
			F.goto "?m=Menus&error=1&msg=" & F.encode("删除失败，当前菜单有子菜单。")
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>