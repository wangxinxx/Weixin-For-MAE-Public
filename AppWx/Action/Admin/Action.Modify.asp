﻿<%
class ActionModify
	private LOGPATH,WX,System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				Mo.assign "System",F.json("{""SystemName"":""管理系统""}")
			else
				set System = L.Read()
				Mo.assign "System",System
			end if
			Mo.assign "subtitle","修改账号密码"
		end if
	end sub
	public sub index
		Mo.assign "NameOld",F.session("wx_auth_name")
		Mo.display "Admin:Modify"
	end sub
	public sub modify
		dim NameOld,AuthOld,Name
		NameOld = F.post.exp("NameOld","^\w+$")
		Name = F.post.exp("Name","^\w+$")
		if NameOld="" then
			F.goto "?m=Modify&error=1&msg=" & F.encode("原用户名错误")
		elseif Name="" then
			F.goto "?m=Modify&error=1&msg=" & F.encode("用户名错误")
		elseif F.session("wx_access_flag")<>"0" and NameOld<>Name then
			F.goto "?m=Modify&error=1&msg=" & F.encode("您不能修改用户名。")
		elseif F.post("AuthOld")="" then
			F.goto "?m=Modify&error=1&msg=" & F.encode("必须输入原密码。")
		else
			if F.session("wx_access_flag")<>"0" then
				Name = F.session("wx_auth_name")
				NameOld = Name
			end if
			dim L : set L = Model__("Authorize","Id").where("Name='" & NameOld & "'").query().fetch()
			if L.Eof() then
				F.goto "?m=Modify&error=1&msg=" & F.encode("用户不存在。")
			else
				dim R : set R = L.Read()
				if R.AllowChangePwd=0 then
					F.goto "?m=Modify&error=1&msg=" & F.encode("不允许修改账户信息。")
					exit sub
				end if
				Mo.Use "SHA1"
				if MoLibSHA1.SHA1(F.md5(MoLibSHA1.SHA2(F.post("AuthOld"))))<>R.Auth then
					F.goto "?m=Modify&error=1&msg=" & F.encode("原密码错误。")
				else
					if F.post("Auth")<>"" then
						if F.post("Auth")<>F.post("AuthRe") then
							F.goto "?m=Modify&error=1&msg=" & F.encode("两次密码输入不一致。")
						else
							Model__("Authorize","Id").where("Name='" & NameOld & "'").update "Name",Name,"Auth",MoLibSHA1.SHA1(F.md5(MoLibSHA1.SHA2(F.post("Auth"))))
							F.goto "?m=Login&a=logout&error=0&msg=" & F.encode("用户修改成功，请牢记新密码。")
						end if
					else
						if Name=NameOld then
							F.goto "?m=Modify&error=0&msg=" & F.encode("没有改变。")
						else
							if Model__("Authorize","Id").where("Name='" & Name & "'").query().fetch().Eof() then
								Model__("Authorize","Id").where("Name='" & NameOld & "'").update "Name",Name
								F.goto "?m=Login&a=logout&error=0&msg=" & F.encode("用户修改成功。")
							else
								F.goto "?m=Modify&error=1&msg=" & F.encode("用户名已存在，没有改变。")
							end if
						end if
					end if
				end if
			end if
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>