﻿<%
class ActionSubscriber
	private System,importcount
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				importcount = 0
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","粉丝管理"
			end if
		end if
	end sub
	public sub index
		Mo.assign "Now",F.timespan()-48*3600
		dim sqlwhere:sqlwhere=""
		if F.get.exp("OpenId","^([\w\-]+)$")<>"" then sqlwhere="OpenId='" & F.get.exp("OpenId","^([\w\-]+)$") & "'"
		Model__("Subscriber","Id").where(sqlwhere).limit(F.get.int("page",1),15).orderby("Id desc").query().assign "Subscriber"
		Mo.display "Admin:Subscriber"
	end sub
	public sub locations
		dim OpenId:OpenId = F.get.exp("OpenId","^([\w\-]+)$")
		Mo.assign "OpenId",OpenId
		Model__("Location","Id").limit(F.get.int("page",1),10).where("OpenId='" & OpenId & "'").orderby("StateDate desc,Id desc").query().assign "Locations"
		Mo.display "Admin:Subscriber_Locations"
	end sub
	public sub view
		dim sqlwhere:sqlwhere=""
		if F.get.int("Id",0)>0 then sqlwhere = "Id=" & F.get.int("Id",0)
		if F.get.safe("OpenId")<>"" then sqlwhere = "OpenId='" & F.get.safe("OpenId") & "'"
		if sqlwhere <>"" then
			dim M: set M = Model__("Subscriber","Id").where(sqlwhere).query()
			if not M.fetch().Eof() then
				Model__("Groups","Id").orderby("GroupId asc").query().assign "Groups"
				M.assign()
			end if
		end if
		Mo.assign "Now",F.timespan()-48*3600
		Mo.display "Admin:Subscriber_Detail"
	end sub
	public sub getusersbyopenid
		dim OpenIds:OpenIds = F.post.exp("openids","^([\w\-\,]+)$")
		if OpenIds="" then
			F.echo "[]"
		else
			OpenIds = "'" & replace(OpenIds,",","','") & "'"
			F.echo Model__("Subscriber","Id").select("NickName,OpenId").where("OpenId in(" & OpenIds & ")").query().getjson()
		end if
	end sub
	public sub getalluser
		Mo.Use "WXServices"
		dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			dim json:set json = WX.getUsers(F.post.safe("OpenId"))
			if json.error then
				F.goto "?m=Subscriber",WX.errmsg
			else
				if json.users<>"" then
					dim openids,openid,i : openids = split(json.users,",")
					for i=0 to ubound(openids)
						if Model__("Subscriber","Id").where("OpenId='" & openids(i) & "'").query().fetch().Eof() then
							Model__("Subscriber","Id").insert "OpenId", openids(i), "Subscribe", 1
						else
							Model__("Subscriber","Id").where("OpenId='" & openids(i) & "'").update "Subscribe",1
						end if
					next
					importcount = importcount + json.count
					if importcount<json.total then
						F.post "OpenId",json.next_openid
						Call getalluser()
					else
						F.goto "?m=Subscriber&error=0&msg=" & F.encode("同步完成。")
					end if
				else
					F.goto "?m=Subscriber&error=0&msg=" & F.encode("同步完成。")
				end if
			end if
		else
			F.goto "?m=Subscriber&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
		end if		
	end sub
	public sub resetmode
		Model__("Subscriber","Id").where("Id=" & F.get.int("Id",0)).update "Mode","none"
		F.echo "对话模式重置成功"
	end sub
	public sub getmygroup
		dim OpenId : OpenId = getuseropenid()
		if OpenId<>"" then
			Mo.Use "WXServices"
			dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
			if WX.getAccessToken() then
				dim json:set json = WX.getusergroup(OpenId)
				if json.error then
					F.echo WX.errmsg
				else
					Model__("Subscriber","Id").where("OpenId='" & OpenId & "'").update "GroupId",json.groupid
					F.echo "分组获取成功。"
				end if
			else
				F.echo "无法获取AccessToken。" & WX.errmsg
			end if
		end if
	end sub
	public sub moveto
		dim OpenId : OpenId = getuseropenid()
		if OpenId<>"" then
			Mo.Use "WXServices"
			dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
			if WX.getAccessToken() then
				dim json:set json = WX.updateusergroup(F.post.int("GroupId",0),OpenId)
				if json.error then
					F.echo WX.errmsg
				else
					Model__("Subscriber","Id").where("OpenId='" & OpenId & "'").update "GroupId",F.post.int("GroupId",0)
					F.echo "分组移动成功。"
				end if
			else
				F.echo "无法获取AccessToken。" & WX.errmsg
			end if
		end if
	end sub
	public sub getuser
		dim OpenId : OpenId = getuseropenid()
		if OpenId<>"" then
			Mo.Use "WXServices"
			dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
			if WX.getAccessToken() then
				dim json:set json = WX.getUser(OpenId)
				if json.error then
					F.echo WX.errmsg
				else
					if json.subscribe=1 then
						Model__("Subscriber","Id").where("OpenId='" & OpenId & "'").update _
						"NickName",json.nickname,_
						"Sex",json.sex,_
						"Language",json.language,_
						"City",json.city,_
						"County",json.country,_
						"Province",json.province,_
						"Headimgurl",json.headimgurl,_
						"SubscribeTime",json.subscribe_time,_
						"Subscribe",1
						F.echo "信息获取成功。"
					else
						Model__("Subscriber","Id").where("OpenId='" & OpenId & "'").update "Subscribe",0
						F.echo "用户未关注微信。"
					end if
				end if
			else
				F.echo "无法获取AccessToken。" & WX.errmsg
			end if
		end if
	end sub
	private function getuseropenid()
		dim id:id = F.get.int("Id",0)
		if id>0 then
			dim L:set L = Model__("Subscriber","Id").where("Id=" & id).query().fetch()
			if L.Eof() then
				F.goto "?m=Subscriber&error=1&msg=" & F.encode("记录不存在。")
				getuseropenid=""
			else
				getuseropenid =L.Read("OpenId")
				if isnull(getuseropenid) then getuseropenid=""
			end if
		else
			F.goto "?m=Subscriber&error=1&msg=" & F.encode("错误的Id。")
			getuseropenid = ""
		end if
	end function
	public sub delete
		Model__("Subscriber","Id").where("Id=" & F.get.int("Id",0)).delete()
		F.goto "?m=Subscriber&error=0&msg=" & F.encode("操作成功")
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>