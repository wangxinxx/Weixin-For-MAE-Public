﻿<%
class ActionMediaMessages
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","常用消息管理"
			end if
		end if
	end sub
	public sub index
		if F.get.exp("ftype","^(text|image|music|news|voice|video|mutinews)$")="" then F.get "ftype","text"
		Model__("MediaMessages","Id").where("MsgType='" & F.get("ftype") & "'").limit(F.get.int("page",1),15).orderby("Id desc").query().assign "Messages"
		Mo.display "Admin:MediaMessages"
	end sub
	public sub getimagesfromurl
		dim url:url = F.get.url("url")
		if url<>"" then
			Mo.Use "HttpRequest"
			dim returnValue : returnValue = MoLibHttpRequest.New(url,"GET","",false).gettext("utf-8")
			if returnValue<>"" then
				dim matches,match:set matches = GetMatch(returnValue,"<img(.*?)src=(\'|"")(.+?)(\'|"")")
				dim all:all="|"
				for each match in matches
					dim src:src = match.submatches(2)
					if RegTest(src,"^http(s)?\:\/\/") then
					elseif left(src,1)="/" then
						src = ReplaceEx(url,"^http(s)?\:\/\/(.+?)\/(.*?)$","http$1://$2") & src
					else
						src = ReplaceEx(url,"^http(s)?\:\/\/(.+?)\/([^\/\?]*?)(.*?)$","http$1://$2/") & src
					end if
					if instr(all,"|" & src & "|")<=0 then all = all & src & "|"
				next
				F.echo all
			end if
		end if
	end sub
	public sub parseurl
		dim url:url = F.get.url("url")
		if url<>"" then
			Mo.Use "HttpRequest"
			dim http : set http = MoLibHttpRequest.New(url,"GET","",false).send()
			dim contenttype :contenttype = http.getheader("content-type")
			dim charset,charset1:charset="utf-8"
			if contenttype<>"" and regtest(contenttype,"charset\=([\w\-]+)") then charset = GetMatch(contenttype,"charset\=([\w\-]+)")(0).submatches(0)
			dim returnValue : returnValue = http.gettext(charset)
			if returnValue<>"" then
				if regtest(returnValue,"<meta(\s*)charset\=""(.+?)""(\s*)\/>") then charset1 = GetMatch(returnValue,"<meta(\s*)charset\=""(.+?)""(\s*)\/>")(0).submatches(1)
				if charset1="" then
					if regtest(returnValue,"\bcontent\=""text\/html\;(\s*)charset\=(.+?)""") then charset1 = GetMatch(returnValue,"\bcontent\=""text\/html\;(\s*)charset\=(.+?)""")(0).submatches(1)
				end if
				if lcase(charset1)<>lcase(charset) then returnValue = http.gettext(charset1)
				dim title,description,images
				if regtest(returnValue,"<title>([\s\S]+?)</title>") then title = GetMatch(returnValue,"<title>([\s\S]+?)</title>")(0).submatches(0)
				if regtest(returnValue,"<meta([^\<]+?)name\=""description""([\s\S]*?)>") then
					description = GetMatch(returnValue,"<meta([^\<]+?)name\=""description""([\s\S]*?)>")(0).value
					if regtest(description,"content\=""(.+?)""") then description = GetMatch(description,"content\=""(.+?)""")(0).submatches(0) else description=""
				end if
				dim matches,match:set matches = GetMatch(returnValue,"<img(.*?)src=(\'|"")(.+?)(\'|"")")
				dim all:all="|"
				for each match in matches
					dim src:src = match.submatches(2)
					if RegTest(src,"^http(s)?\:\/\/") then
					elseif left(src,1)="/" then
						src = ReplaceEx(url,"^http(s)?\:\/\/(.+?)\/(.*?)$","http$1://$2") & src
					else
						src = ReplaceEx(url,"^http(s)?\:\/\/(.+?)\/([^\/\?]*?)(.*?)$","http$1://$2/") & src
					end if
					if instr(all,"|" & src & "|")<=0 then all = all & src & "|"
				next
				
				if description="" then
					returnValue = ReplaceEx(returnValue,"<title>([\s\S]+?)</title>","")
					returnValue = ReplaceEx(returnValue,"<style(.*?)>([\s\S]+?)</style>","")
					returnValue = ReplaceEx(returnValue,"<script(.*?)>([\s\S]+?)</script>","")
					returnValue = ReplaceEx(returnValue,"<([\s\S]*?)>","")
					returnValue = ReplaceEx(returnValue,"(\s+)","")
					description = left(returnValue,200)
				end if
				F.echo "{""Title"":""" & F.jsEncode(title) & """,""Description"":""" & F.jsEncode(description) & """,""All"":""" & F.jsEncode(all) & """,""Charset"":""" & F.jsEncode(charset1) & """}"
				exit sub
			end if
		end if
		F.echo "{""Title"":"""",""Description"":"""",""All"":"""",""Charset"":""""}"
	end sub
	public sub edit
		dim timespannow:timespannow = F.timespan()-259200
		dim Id:Id = F.get.int("Id",0)
		if Id>0 then
			dim R:set R = Model__("MediaMessages","Id").where("Id=" & Id).query().fetch().Read()
			Mo.assign "MsgType",R.MsgType
			Mo.assign "Id",Id
			Mo.assign "Name",R.Name
			if R.MsgType="mutinews" then
				Mo.assign "Details",R.Details
			else
				Mo.assign "Details",F.json(R.Details)
			end if
			F.get "type",R.MsgType
		else
			Mo.assign "Id",0
		end if
		if F.get("type")="image" then Model__("Media","Id").where("Type='image' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="voice" then Model__("Media","Id").where("Type='voice' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="video" then Model__("Media","Id").where("Type='video' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="music" then Model__("Media","Id").where("Type='thumb' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="mutinews" then Model__("MediaMessages","Id").where("MsgType='news'").orderby("Id desc").query().assign "News"
		if F.get.exp("type","^(text|image|music|news|voice|video|mutinews)$")="" then F.get "type","text"
		Mo.display "Admin:MediaMessagesEdit"
	end sub
	public sub savemessage
		dim Json,details,Id:Id = F.post.int("Id",0)
		Mo.Use "JsonGenerater"
		set Json = MoLibJsonGenerater.New()
		details="1"
		select case F.post("Type")
			case "text"
				Json.put "Content",F.post("Content")
			case "voice"
				Json.put "MediaId",F.post("Mediaid")
			case "image" 
				Json.put "MediaId",F.post("Mediaid")
			case "news"
				Json.put "Title",F.post("Title")
				Json.put "Content",F.post("Content")
				Json.put "Url",F.post("Url")
				Json.put "PicUrl",F.post("PicUrl")
			case "video"
				Json.put "Title",F.post("Title")
				Json.put "Description",F.post("Description")
				Json.put "Mediaid",F.post("Mediaid")
			case "music"
				Json.put "Mediaid",F.post("Mediaid")
				Json.put "MusicUrl",F.post("MusicUrl")
				Json.put "Title",F.post("Title")
				Json.put "Description",F.post("Description")
				Json.put "HQMusicUrl",F.post("HQMusicUrl")
			case "mutinews"
				details = F.post.intList("NewsId","")
				details = replace(details," ","")
			case else
				details=""
		end select
		if details="" then
			F.echo "不支持的消息类型，或您在添加多条图文时未选择任何新闻。"
		else
			if details="1" then details = Json.toString()
			If Id>0 then
				Model__("MediaMessages","Id").where("Id=" & Id).update _
				"Name",F.post.safe("Name"),_
				"MsgType",F.post.exp("Type","^(text|image|music|news|voice|video|mutinews)$"),_
				"CreateTime",F.timespan(),_
				"Details",details
			else
				Model__("MediaMessages","Id").insert _
				"Name",F.post.safe("Name"),_
				"MsgType",F.post.exp("Type","^(text|image|music|news|voice|video|mutinews)$"),_
				"CreateTime",F.timespan(),_
				"Details",details
			end if
			F.echo "消息已保存。"
		end if
	end sub
	public sub delete
		if not Model__("ResponseRules","Id").where("MsgId=" & F.get.int("Id",0)).query().fetch().Eof() then
			F.goto "?m=MediaMessages&ftype=" & F.get("ftype") & "&error=1&msg=" & F.encode("无法删除，本消息正在被使用。")
		else
			Model__("MediaMessages","Id").where("Id=" & F.get.int("Id",0)).delete()
			F.goto "?m=MediaMessages&ftype=" & F.get("ftype") & "&error=0&msg=" & F.encode("操作成功")
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>