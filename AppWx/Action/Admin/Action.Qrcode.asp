﻿<%
class ActionQrcode
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","二维码管理"
			end if
		end if
	end sub
	public sub index
		'Model__("QRcode","Id").where("Expired>0 and CreateAt+Expired<" & F.timespan()).delete()
		Mo.assign "Now",F.timespan()
		Model__("QRcode","Id").select("*,(CreateAt + Expired) as ExpiredDate").limit(F.get.int("page",1),10).orderby("CreateAt desc").query().assign "QRCodes"
		Mo.display "Admin:Qrcode"
	end sub
	public sub DeleteQrcode
		Model__("QRcode","Id").where("Id=" & F.get.int("Id",0)).delete()
		if Model__.lastRows>0 then
			F.goto "?m=Qrcode&error=0&msg=" & F.encode("操作成功")
		else
			F.goto "?m=Qrcode&error=0&msg=" & F.encode("操作成功，但未影响任何行。")
		end if
	end sub
	public sub getqrcode
		dim SenseId : SenseId = F.post.int("SenseId",0)
		if SenseId=0 then
			F.goto "?m=Qrcode&error=1&msg=" & F.encode("SenseId为空")
		elseif not Model__("QRcode","Id").where("SenseId=" & SenseId & " and ((Expired>0 and CreateAt+Expired>=" & F.timespan() & ") or Expired=0)").query().fetch().Eof() then
			F.goto "?m=Qrcode&SenseId=" & SenseId & "&error=1&msg=" & F.encode("已存在相同的SenseId[" & SenseId & "]")
		else
			Mo.Use "WXServices"
			dim WX : set WX = MoLibWXServices.New(System.AppID,System.AppSecret)
			if WX.getAccessToken() then
				dim json1
				if F.post("act")="生成临时二维码" then
					if SenseId<=100000 then
						set json1 = F.json("{""error"":true}")
						WX.errmsg="临时二维码必须大于100000"
					else
						set json1 = WX.createqrcode(SenseId,F.post.int("Expired",1800))
					end if
				else
					F.post "Expired",0
					set json1 = WX.createlimitqrcode(SenseId)
				end if
				if json1.error then
					F.goto "?m=QRcode&error=1&msg=" & F.encode(WX.errmsg)
				else
					F.post "CreateAt",F.timespan()
					F.post "Ticket",json1.ticket
					F.post.remove "act"
					Model__("QRcode","Id").insert _
					"Name",F.post.safe("Name"),_
					"SenseId",SenseId,_
					"CreateAt",F.timespan(),_
					"Ticket",json1.ticket,_
					"Expired",F.post.int("Expired",1800)
					F.goto "?m=Qrcode&error=0&msg=" & F.encode("成功生成二维码")
				end if
			else
				F.goto "?m=Qrcode&error=1&msg=" & F.encode("Token获取失败:" & WX.errmsg)
			end if
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>