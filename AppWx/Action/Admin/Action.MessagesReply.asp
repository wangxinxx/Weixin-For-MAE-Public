﻿<%
class ActionMessagesReply
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.exit "请登录。"
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.exit "找不到微信配置。"
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","消息回复"
			end if
		end if
	end sub
	public sub index
		dim timespannow:timespannow = F.timespan()-259200
		if F.get("type")="image" then Model__("Media","Id").where("Type='image' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="voice" then Model__("Media","Id").where("Type='voice' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="video" then Model__("Media","Id").where("Type='video' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="music" then Model__("Media","Id").where("Type='thumb' and CreatedAt>=" & timespannow).orderby("Id desc").query().assign "Media"
		if F.get("type")="mutinews" then Model__("MediaMessages","Id").where("MsgType='news'").orderby("Id desc").query().assign "News"
		if F.get.exp("type","^(text|image|music|news|voice|video|mutinews)$")="" then F.get "type","text"
		Mo.display "Admin:MessagesReply"
	end sub
	public sub sendmessage
		if not (System.Wx_Acount_Type="服务号" and System.Wx_Acount_Authed =1) then
			F.echo "当前账号不支持本功能。"
		else
			Mo.Use "WXServicesKf"
			dim WX,json1,details,Openid,CreateTime,Json,All : set WX = MoLibWXServicesKf.New(System.AppID,System.AppSecret)
			Openid = F.post.safe("OpenId")
			CreateTime = F.post.int("CreateTime",0)
			All = F.post.exp("All","^(yes)$")
			if WX.getAccessToken() then
				Mo.Use "JsonGenerater"
				set Json = MoLibJsonGenerater.New()
				select case F.post("Type")
					case "text"
						details = F.post("Content")
						Json.put "Content",details
						set json1 = WX.sendtext(Openid,details)
					case "voice"
						details = F.post("Mediaid")
						Json.put "MediaId",details
						set json1 = WX.sendvoice(Openid,details)
					case "image" 
						details = F.post("Mediaid")
						Json.put "MediaId",details
						set json1 = WX.sendimage(Openid,details)
					case "news"
						WX.News.clear()
						WX.News.append F.post("Title"),F.post("Content"),F.post("Url"),F.post("PicUrl")
						set json1 = WX.sendnews(Openid)
						Json.put "Title",F.post("Title")
						Json.put "Content",F.post("Content")
						Json.put "Url",F.post("Url")
						Json.put "PicUrl",F.post("PicUrl")
					case "mutinews"
						dim newslist,i:newslist = split(F.post.intList("NewsId",0),",")
						parseMediaMessage = ""
						WX.News.clear()
						if Ubound(newslist)>=0 then
							dim nlist:set nlist = Json.putnewarray("List")
							for i=0 to Ubound(newslist)
								dim L:set L = Model__("MediaMessages","Id").where("MsgType='News' and Id =" & newslist(i) & "").query().fetch()
								if not L.Eof() then
									dim da:set da = F.json(L.Read().Details)
									WX.News.append da.Title,da.Content,da.Url,da.PicUrl
									nlist.put da
								end if
							next
						end if
						set json1 = WX.sendnews(Openid)
					case "video"
						Json.put "Title",F.post("Title")
						Json.put "Description",F.post("Description")
						Json.put "Mediaid",F.post("Mediaid")
						set json1 = WX.sendvideo(Openid,F.post("Mediaid"),F.post("Title"),F.post("Description"))
					case "music"
						Json.put "Mediaid",F.post("Mediaid")
						Json.put "MusicUrl",F.post("MusicUrl")
						Json.put "Title",F.post("Title")
						Json.put "Description",F.post("Description")
						Json.put "HQMusicUrl",F.post("HQMusicUrl")
						set json1 = WX.sendmusic(Openid,F.post("Mediaid"),F.post("MusicUrl"),F.post("Title"),F.post("Description"),F.post("HQMusicUrl"))
					case else
						set json1 = F.json("{""error"":true}")
						WX.errmsg = "不支持的消息类型。"
				end select
				if json1.error then
					F.echo WX.errmsg
				else
					Model__("Messages","Id").insert _
					"MsgType",F.post("Type"),_
					"ToUserName",Openid,_
					"FromUserName",System.Wx_Acount_SrcId,_
					"CreateTime",F.timespan(),_
					"MsgId",0,_
					"Details",Json.toString(),_
					"ForMessage",CreateTime
					'if CreateTime>0 then Model__("Messages","Id").where("FromUserName='" & Openid & "' and CreateTime=" & CreateTime).update "IsReply",1
					F.echo "消息已发送。"
				end if
			else
				F.echo "无法获取AccessToken。" & WX.errmsg
			end if
		end if
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>