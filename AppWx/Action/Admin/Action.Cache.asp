﻿<%
class ActionCache
	private LOGPATH,WX,System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		end if
	end sub
	public sub index
		F.echo "清理编译缓存：" & Mo.ClearCompiledCache(), true
		F.echo "清理模块缓存：" & Mo.ModelCacheClear(), true
		F.echo "清理类库缓存：" & Mo.ClearLibraryCache(), true
		if F.server("HTTP_REFERER")<>"" then F.goto F.server("HTTP_REFERER") else F.goto "?m=Admin"
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>