﻿<%
class ActionMessagesMuti
	private System
	private sub Class_Initialize()
		if F.session("wx_auth_name")="" then
			F.redirect "?m=login&msg=" & F.encode("请登录。")
		else
			dim L : set L = Model__("System","Id").where("Tag=1").query().fetch()
			if L.Eof() then
				F.redirect "?m=Admin&error=1&msg=" & F.encode("找不到微信配置。")
			else
				set System = L.Read()
				Mo.assign "WxSrcId",System.Wx_Acount_SrcId
				Mo.assign "System",System
				Mo.assign "subtitle","多媒体消息管理"
			end if
		end if
	end sub
	public sub index
		if F.get("type")="image" then Model__("Media","Id").where("Type='image'").orderby("Id desc").query().assign "Media"
		if F.get("type")="voice" then Model__("Media","Id").where("Type='voice'").orderby("Id desc").query().assign "Media"
		if F.get("type")="video" then Model__("MediaMuti","Id").where("Type='video'").orderby("Id desc").query().assign "Media"
		if F.get("type")="news" then Model__("MediaMuti","Id").where("Type='news'").orderby("Id desc").query().assign "Media"
		if F.get.exp("type","^(text|image|news|voice|video)$")="" then F.get "type","text"
		Model__("Groups","Id").orderby("Id desc").query().assign "Groups"
		Model__("MessagesMuti","Id")_
		.limit(F.get.int("page",1),10)_
		.orderby("Id desc")_
		.query()_
		.assign "MessagesMuti"
		
		Mo.display "Admin:MessagesMuti"
	end sub
	public sub sendmessagemuti
		Mo.Use "WXServicesMuti"
		dim WX,json1 : set WX = MoLibWXServicesMuti.New(System.AppID,System.AppSecret)
		if WX.getAccessToken() then
			dim sendto,details : sendto = F.post.exp("SendTo","^(group|users)$")
			WX.sendto =sendto
			if sendto = "group" then WX.filter = F.post("GroupId")
			if sendto = "users" then
				WX.filter = F.post("Users")
				WX.filter = ReplaceEx(Replace(WX.filter,vbcr,vblf),"(\n+)",",")
			end if
			select case F.post("Type")
				case "text"
					details = F.post("Content")
					set json1 = WX.sendtext(details)
				case "news"
					details = F.post("Mediaid")
					set json1 = WX.sendnews(details)
				case "voice"
					details = F.post("Mediaid")
					set json1 = WX.sendvoice(details)
				case "image" 
					details = F.post("Mediaid")
					set json1 = WX.sendimage(details)
				case "video"
					details = F.post("Mediaid") & ", " & F.post("Title") & ", " & F.post("Description")
					set json1 = WX.sendtext(F.post("Mediaid"),F.post("Title"),F.post("Description"))
				case else
					set json1 = F.json("{""error"":true}")
					WX.errmsg = "不支持的群发类型。"
			end select
			if json1.error then
				F.goto "?m=MessagesMuti&error=1&msg=" & F.encode(WX.errmsg)
			else
				Model__("MessagesMuti","Id").insert _
				"Type",F.post("Type"),_
				"SendTo",sendto,_
				"Filter",WX.filter,_
				"Details",details,_
				"MsgId",json1.msg_id,_
				"CreateAt",F.timespan()
				F.goto "?m=MessagesMuti&error=0&msg=" & F.encode("群发申请成功提交。")
			end if
		else
			F.goto "?m=MessagesMuti&error=1&msg=" & F.encode("无法获取AccessToken。" & WX.errmsg)
		end if
	end sub
	public sub delete
		Model__("MessagesMuti","Id").where("Id=" & F.get.int("Id",0)).delete()
		F.goto "?m=MessagesMuti&error=0&msg=" & F.encode("操作成功")
	end sub
	public sub [empty](a)
		F.goto "?m=" & Mo.Method & "&error=1&msg=" & F.encode("不支持的方法：" & a & "。")
	end sub
end class
%>