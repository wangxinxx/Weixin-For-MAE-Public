﻿<%
class ActionNotice
	private LOGPATH,WX,System
	private sub Class_Initialize()
		LOGPATH = F.mappath("/AppWx/Action/request.log")
		dim L : set L = Model__("System","Id").where("Status=1 and Tag=1").query().fetch()
		if L.Eof() then
			F.exit "配置异常"
		else
			set System = L.Read()
		end if
	end sub
	public sub index
		Mo.Use "WXMessage"
		Mo.Use "File"
		dim requeststr,responsestr,timespan,WxDelegate,WxDelegateChat
		responsestr = ""
		set WX = MoLibWXMessage.New()
		WX.SignManager.token=System.Token
		WX.SignManager.timestamp = F.get("timestamp")
		WX.SignManager.nonce = F.get("nonce")
		if WX.SignManager.comparewith(F.get("signature")) then
			if F.server("REQUEST_METHOD")="GET" then F.exit F.get("echostr")
			requeststr = F.string.frombinary(request.binaryread(4096),"utf-8")
			LOGS requeststr
			if not WX.loadRequest(requeststr) then
				LOGS "请求解析失败。"
			else
				timespan = F.timespan()
				set WxDelegate = Mo.A("Delegate")
				set WxDelegate.mSystem = System
				set WxDelegate.mWX = WX
				WxDelegate.mTimespan = timespan
				'当消息到达时
				WxDelegate.OnMessageArrived()
				if WX.MsgType="event" then
					'事件委托
					responsestr = WxDelegate.OnEvent()
				else
					'聊天委托
					set WxDelegateChat = Mo.A("DelegateChat")
					set WxDelegateChat.mSystem = System
					set WxDelegateChat.mWX = WX
					WxDelegateChat.mTimespan = timespan
					responsestr = WxDelegateChat.OnChat()
				end if
				if responsestr<>"" then
					F.echo responsestr
					WxDelegate.OnResponseSent(responsestr)
					LOGS responsestr
				end if
			end if
		else
			LOGS "艾玛，错了。" & Request.querystring
			F.echo "艾玛，错了。"
		end if
	end sub
	public sub [empty](a)
		F.echo "不存在的action：" & a
	end sub
	public sub LOGS(content)
		'MoLibFile.appendtext LOGPATH,"[" & now() & "]=" & F.server("REMOTE_ADDR") & "，" & content & vbcrlf & vbcrlf
	end sub
end class
%>