﻿<%
class ActionDelegateChat
	private WX,System,Timespan
	private sub Class_Initialize()
	end sub
	public property set mSystem(value)
		set System = value
	end property
	public property set mWX(value)
		set WX = value
	end property
	public property let mTimespan(value)
		Timespan = value
	end property

	private function getResponse(delegate)
		dim L : set L = Model__("ResponseRules","Id").where("Disabled=0 and Delegate='" & delegate & "'").orderby("Priority desc,MatchType desc").query().fetch()
		if L.Eof() then set L = Model__("ResponseRules","Id").where("Disabled=0 and Delegate='msg_default'").orderby("Priority desc,MatchType desc").query().fetch()
		if L.Eof() then set L = Model__("ResponseRules","Id").where("Disabled=0 and Delegate='global_default'").orderby("Priority desc,MatchType desc").query().fetch()
		set getResponse = L
	end function

	private function parseMediaMessage(byref R)
		'R.MsgType/R.Details
		dim Json
		if instr(R.Details,"{")>0 then set Json = F.json(R.Details)
		if R.MsgType="text" then
			parseMediaMessage = WX.createTextResponse(Json.Content)
		elseif R.MsgType="image" then
			parseMediaMessage = WX.createImageResponse(Json.MediaId)
		elseif R.MsgType="voice" then
			parseMediaMessage = WX.createVoiceResponse(Json.MediaId)
		elseif R.MsgType="video" then
			parseMediaMessage = WX.createVideoResponse(Json.MediaId,Json.Title,Json.Description)
		elseif R.MsgType="music" then
			parseMediaMessage = WX.createMusicResponse(Json.Mediaid,Json.Title,Json.Description,Json.MusicUrl,Json.HQMusicUrl)
		elseif R.MsgType="news" then
			WX.News.clear()
			WX.News.append Json.Title,Json.Content,Json.Url,Json.PicUrl
			parseMediaMessage = WX.createNewsResponse()
		elseif R.MsgType="mutinews" then
			dim newslist,i:newslist = split(R.Details,",")
			parseMediaMessage = ""
			if Ubound(newslist)>=0 then
				WX.News.clear()
				for i=0 to Ubound(newslist)
					dim L:set L = Model__("MediaMessages","Id").where("MsgType='News' and Id =" & newslist(i) & "").query().fetch()
					if not L.Eof() then
						dim da:set da = F.json(L.Read().Details)
						WX.News.append da.Title,da.Content,da.Url,da.PicUrl
					end if
				next
				parseMediaMessage = WX.createNewsResponse()
			end if
		end if
	end function

	private function parseMsgId(Byref R)
		if isnumeric(R.MsgId) then
			dim L : set L = Model__("MediaMessages","Id").where("Id=" & R.MsgId).query().fetch()
			if L.Eof() then
				parseMsgId=""
			else
				parseMsgId = parseMediaMessage(L.Read())
			end if
		elseif R.MsgId="TEXT" then
			parseMsgId = WX.createTextResponse(R.Details)
		else
			dim Fn:Fn = getFnByMsgId(R.MsgId)
			if Fn(0)="" then
				parseMsgId = Mo(R.MsgId)(WX,System)
			else
				parseMsgId = Eval("Mo(Fn(1))." & Fn(0) & "(WX,System)")
			end if
		end if
	end function
	private function getFnByMsgId(byval MsgId)
		dim ary:ary = Split(MsgId,".")
		if Ubound(ary)=2 then
			getFnByMsgId = array(ary(2),Left(MsgId,instrrev(MsgId,".")-1))
		else
			getFnByMsgId = array("",MsgId)
		end if
	end function
	private function OnText(Content)
		'接收到文本信息时
		dim RM : set RM = getResponse("msg_text")
		if not RM.Eof() then
			dim R,MsgId,Result
			MsgId = ""
			Result=""
			do while not RM.Eof()
				set R = RM.Read()
				MsgId = ""
				if R.MatchType=1 then
					if R.Keyword=Content then MsgId = R.MsgId
				else
					if R.Keyword=Content then
						MsgId = R.MsgId
					elseif instr(Content,R.Keyword) then
						MsgId = R.MsgId
					elseif RegTest(Content,R.Keyword) then
						MsgId = R.MsgId
					end if
				end if
				if MsgId<>"" then
					Result = parseMsgId(R)
					if Result<>"" then exit do
				end if
			loop
			if Result="" then exit function
			OnText = Replace(Result,"{$Content}",Content)
		end if
	end function
	private function OnImage(MediaId,PicUrl)
		'接收到图片信息时
		dim RM : set RM = getResponse("msg_image")
		if not RM.Eof() then
			OnImage = parseMsgId(RM.Read())
			OnImage = Replace(OnImage,"{$MediaId}",MediaId)
			OnImage = Replace(OnImage,"{$PicUrl}",PicUrl)
		end if
		if System.Save_Funs_Msg=1 then Model__("Media","Id").insert "Name","来自图片消息","Type","image","MediaId",MediaId,"CreatedAt",F.timespan(),"LocalPath",PicUrl,"SrcId", System.Wx_Acount_SrcId
	end function
	private function OnVoice(Format,MediaId,Recognition)
		'接收到语音信息时
		dim RM : set RM = getResponse("msg_voice")
		if not RM.Eof() then
			OnVoice = parseMsgId(RM.Read())
			OnVoice = Replace(OnVoice,"{$Format}",Format)
			OnVoice = Replace(OnVoice,"{$MediaId}",MediaId)
			OnVoice = Replace(OnVoice,"{$Recognition}",Recognition)
		end if
		if System.Save_Funs_Msg=1 then Model__("Media","Id").insert "Name","来自语音消息","Type","voice","MediaId",MediaId,"CreatedAt",F.timespan(),"LocalPath",Recognition,"SrcId", System.Wx_Acount_SrcId
	end function
	private function OnVideo(ThumbMediaId,MediaId)
		'接收到视频信息时
		dim RM : set RM = getResponse("msg_video")
		if not RM.Eof() then
			OnVideo = parseMsgId(RM.Read())
			OnVideo = Replace(OnVideo,"{$ThumbMediaId}",ThumbMediaId)
			OnVideo = Replace(OnVideo,"{$MediaId}",MediaId)
		end if
		if System.Save_Funs_Msg=1 then 
			Model__("Media","Id").insert "Name","来自视频消息","Type","video","MediaId",MediaId,"CreatedAt",F.timespan(),"SrcId", System.Wx_Acount_SrcId
			Model__("Media","Id").insert "Name","来自视频消息","Type","thumb","MediaId",ThumbMediaId,"CreatedAt",F.timespan(),"SrcId", System.Wx_Acount_SrcId
		end if
	end function
	private function OnLocation(Location_X,Location_Y,Scale,Label)
		'接收到位置信息时
		dim RM : set RM = getResponse("msg_location")
		if not RM.Eof() then
			OnLocation = parseMsgId(RM.Read())
			OnLocation = Replace(OnLocation,"{$Location_X}",Location_X)
			OnLocation = Replace(OnLocation,"{$Location_Y}",Location_Y)
			OnLocation = Replace(OnLocation,"{$Scale}",Scale)
			OnLocation = Replace(OnLocation,"{$Label}",Label)
		end if
	end function
	private function OnLink(Title,Description,Url)
		'接收到链接信息时
		dim RM : set RM = getResponse("msg_link")
		if not RM.Eof() then
			OnLink = parseMsgId(RM.Read())
			OnLink = Replace(OnLink,"{$Title}",Title)
			OnLink = Replace(OnLink,"{$Description}",Description)
			OnLink = Replace(OnLink,"{$Url}",Url)
		end if
		if System.Save_Funs_Msg=1 then
			Model__("MediaMessages","Id").insert _
			"Name",Title,_
			"MsgType","news",_
			"CreateTime",F.timespan(),_
			"SrcId", System.Wx_Acount_SrcId,_
			"Details","{""Title"":""" & F.jsEncode(Title) & """,""Content"":""" & F.jsEncode(Description) & """,""Url"":""" & F.jsEncode(Url) & """,""PicUrl"":""""}"
		end if
	end function
	
	public function OnChat()
		Call CheckState()
		if WX.MsgType="text" then
			OnChat = OnText(WX.Request.Content)
		elseif WX.MsgType="image" then
			OnChat = OnImage(WX.Request.MediaId,WX.Request.PicUrl)
		elseif WX.MsgType="voice" then
			OnChat = OnVoice(WX.Request.Format,WX.Request.MediaId,WX.Request.Recognition)
		elseif WX.MsgType="video" then
			OnChat = OnVideo(WX.Request.ThumbMediaId,WX.Request.MediaId)
		elseif WX.MsgType="location" then
			OnChat = OnLocation(WX.Request.Location_X,WX.Request.Location_Y,WX.Request.Scale,WX.Request.Label)
		elseif WX.MsgType="link" then
			OnChat = OnLink(WX.Request.Title,WX.Request.Description,WX.Request.Url)
		else
			OnChat = ""
		end if
	end function
	private sub CheckState
		if not (isobject(System) and isobject(WX)) then F.exit "Illegal call"
	end sub
end class
%>